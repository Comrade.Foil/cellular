#include "src/App.h"

// ImGui integration
// proper blobs morphing
// bloom effect
// collision detection
//

using namespace std;
using namespace glm;

int main() {
	AppConfig config;

	App app(config);

	app.setup();
	app.start();

	exit(EXIT_SUCCESS);
}
