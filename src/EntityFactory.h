#ifndef CELLULAR_ENTITYFACTORY_H
#define CELLULAR_ENTITYFACTORY_H

#include "entt/entity/registry.hpp"
#include "glm/vec3.hpp"

#include "components/CDebugLine.h"
#include "components/CTransform.h"

using namespace Component;
using namespace glm;

namespace EntityFactory {
	entt::entity makeDebugLine(entt::registry &reg, const vec3& p1, const vec3& p2, const vec4& c1, const vec4& c2) {
		const entt::entity e = reg.create();
		
		reg.emplace<CDebugLine>(e, p1, p2, c1, c2);
		reg.emplace<CTransform>(e);
		
		return e;
	}
	
	entt::entity makeDebugLine(entt::registry &reg, const CDebugLine& descriptor) {
		const entt::entity e = reg.create();
		
		reg.emplace<CDebugLine>(e, descriptor);
		reg.emplace<CTransform>(e);
		
		return e;
	}
}

#endif //CELLULAR_ENTITYFACTORY_H
