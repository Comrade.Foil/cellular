#include "Window.h"
#include "../imgui/imgui.h"
#include "../imgui/imgui_impl_glfw.h"
#include "../imgui/imgui_impl_opengl3.h"

string Window::MODULE_NAME = "Window";

static void glfwKeyCallback(GLFWwindow *window, int key, int scancode, int action, int mods) {
	if (key == GLFW_KEY_ESCAPE && action == GLFW_PRESS)
		glfwSetWindowShouldClose(window, GLFW_TRUE);
}

static void glfwErrorCallback(int error, const char *description) {
	LOG_MODULE(LOG_ERROR, Window::MODULE_NAME) << to_string(error) << description << LOG_END;
}

static void glDebugCallback(
	GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, GLchar const *message,
	void const *user_param) {
	auto source_str = [source]() -> std::string {
		switch (source) {
			case GL_DEBUG_SOURCE_API:
				return "API";
			case GL_DEBUG_SOURCE_WINDOW_SYSTEM:
				return "WINDOW SYSTEM";
			case GL_DEBUG_SOURCE_SHADER_COMPILER:
				return "SHADER COMPILER";
			case GL_DEBUG_SOURCE_THIRD_PARTY:
				return "THIRD PARTY";
			case GL_DEBUG_SOURCE_APPLICATION:
				return "APPLICATION";
			case GL_DEBUG_SOURCE_OTHER:
				return "OTHER";
			default:
				return "UNKNOWN";
		}
	}();
	
	auto type_str = [type]() {
		switch (type) {
			case GL_DEBUG_TYPE_ERROR:
				return "ERROR";
			case GL_DEBUG_TYPE_DEPRECATED_BEHAVIOR:
				return "DEPRECATED_BEHAVIOR";
			case GL_DEBUG_TYPE_UNDEFINED_BEHAVIOR:
				return "UNDEFINED_BEHAVIOR";
			case GL_DEBUG_TYPE_PORTABILITY:
				return "PORTABILITY";
			case GL_DEBUG_TYPE_PERFORMANCE:
				return "PERFORMANCE";
			case GL_DEBUG_TYPE_MARKER:
				return "MARKER";
			case GL_DEBUG_TYPE_OTHER:
				return "OTHER";
			default:
				return "UNKNOWN";
		}
	}();
	
	auto severity_str = [severity]() {
		switch (severity) {
			case GL_DEBUG_SEVERITY_NOTIFICATION:
				return "NOTIFICATION";
			case GL_DEBUG_SEVERITY_LOW:
				return "LOW";
			case GL_DEBUG_SEVERITY_MEDIUM:
				return "MEDIUM";
			case GL_DEBUG_SEVERITY_HIGH:
				return "HIGH";
			default:
				return "UNKNOWN";
		}
	}();
	
	LOG_INFO << source_str << ", "
			 << type_str << ", "
			 << severity_str << ", "
			 << id << ": "
			 << message << LOG_END;
}

static void printDeviceParameters() {
	// get version info
	const GLubyte *renderer = glGetString(GL_RENDERER); // get renderer string
	const GLubyte *version = glGetString(GL_VERSION); // version as a string
	int workGroupCount[3];
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 0, &workGroupCount[0]);
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 1, &workGroupCount[1]);
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_COUNT, 2, &workGroupCount[2]);
	
	LOG_INFO << "Renderer: " << renderer << LOG_END;
	LOG_INFO << "OpenGL version supported: " << version << LOG_END;
	LOG_INFO << "Max global (total) work group counts:"
			 << " x:" << workGroupCount[0]
			 << " y:" << workGroupCount[1]
			 << " z:" << workGroupCount[2] << LOG_END;
	
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 0, &workGroupCount[0]);
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 1, &workGroupCount[1]);
	glGetIntegeri_v(GL_MAX_COMPUTE_WORK_GROUP_SIZE, 2, &workGroupCount[2]);
	LOG_INFO << "Max local size dimensions:"
			 << " x:" << workGroupCount[0]
			 << " y:" << workGroupCount[1]
			 << " z:" << workGroupCount[2] << LOG_END;
	
	glGetIntegerv(GL_MAX_COMPUTE_SHARED_MEMORY_SIZE, &workGroupCount[0]);
	LOG_INFO << "Max compute shared memory size: " << workGroupCount[0] << LOG_END;
}

Window::Window(const AppConfig& config) {
	glDebugEnabled = config.isGLDebug;
	
	this->initGLFW(config);
	this->initGLEW();
	this->initImGui();
}

Window::~Window() {
	ImGui_ImplOpenGL3_Shutdown();
	ImGui_ImplGlfw_Shutdown();
	ImGui::DestroyContext();
	
	glfwTerminate();
	glfwDestroyWindow(windowContext);
}

void Window::setClearColor(float x, float y, float z, float a) {
	this->color = glm::vec4(x, y, z, a);
}

bool Window::shouldClose() {
	return glfwWindowShouldClose(windowContext);
}

void Window::update() {
	this->dTime = (glfwGetTime() - lastUpdateTime);
	this->lastUpdateTime = glfwGetTime();
	this->globalTime = glfwGetTime() - startUpTime;
	
	this->updateImGui();
	this->resizeFrameBuffer();
	
	glfwSwapBuffers(windowContext);
	
	glClearColor(color.x, color.y, color.z, color.a);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
	glfwPollEvents();
}

void Window::resizeFrameBuffer() {
	int width, height;
	glfwGetFramebufferSize(windowContext, &width, &height);
	glViewport(0, 0, width, height);

	glfwSetWindowSize(windowContext, width, height);
}

void Window::initGLFW(const AppConfig& config) {
	if (!glfwInit()) {
		LOG_MODULE(LOG_ERROR, MODULE_NAME) << "Could not start GLFW3" << LOG_END;
	}
	
	if (!glfwInit()) {
		exit(EXIT_FAILURE);
	}
	
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	glfwWindowHint(GLFW_SAMPLES, 4);
	
	glfwSetErrorCallback(glfwErrorCallback);
	
	windowContext = glfwCreateWindow(config.windowWidth, config.windowHeight, "C3LL", NULL, NULL);
	
	if (!windowContext) {
		LOG_MODULE(LOG_ERROR, MODULE_NAME) << "Could not open m_window with GLFW3" << LOG_END;
		
		glfwTerminate();
	}
	
	glfwSetKeyCallback(windowContext, glfwKeyCallback);
	
	glfwMakeContextCurrent(windowContext);
	
	glfwSetInputMode(windowContext, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
	glfwSetInputMode(windowContext, GLFW_STICKY_KEYS, GL_TRUE);
	
	startUpTime = glfwGetTime();
	lastUpdateTime = glfwGetTime();
	dTime = glfwGetTime() / 1000;
	globalTime = 0;
}

void Window::initImGui() {
	// Decide GL+GLSL versions
#if defined(IMGUI_IMPL_OPENGL_ES2)
	// GL ES 2.0 + GLSL 100
    const char* glsl_version = "#version 100";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
    glfwWindowHint(GLFW_CLIENT_API, GLFW_OPENGL_ES_API);
#elif defined(__APPLE__)
	// GL 3.2 + GLSL 150
    const char* glsl_version = "#version 150";
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 2);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
    glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);            // Required on Mac
#else
	// GL 3.0 + GLSL 130
	const char* glsl_version = "#version 130";
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	//glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);  // 3.2+ only
#endif
	
	// Setup Dear ImGui context
	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;       // Enable Keyboard Controls
	//io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
	io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;           // Enable Docking
	io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;         // Enable Multi-Viewport / Platform Windows
	//io.ConfigViewportsNoAutoMerge = true;
	//io.ConfigViewportsNoTaskBarIcon = true;
	
	// Setup Dear ImGui style
	ImGui::StyleColorsDark();
	//ImGui::StyleColorsLight();
	
	// When viewports are enabled we tweak WindowRounding/WindowBg so platform windows can look identical to regular ones.
	ImGuiStyle& style = ImGui::GetStyle();
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
	{
		style.WindowRounding = 0.0f;
		style.Colors[ImGuiCol_WindowBg].w = 1.0f;
	}
	
	// Setup Platform/Renderer backends
	ImGui_ImplGlfw_InitForOpenGL(windowContext, true);
	ImGui_ImplOpenGL3_Init(glsl_version);
}

void Window::updateImGui() {
	// Start the Dear ImGui frame
	ImGui_ImplOpenGL3_NewFrame();
	ImGui_ImplGlfw_NewFrame();
	ImGui::NewFrame();
	
	{
		static float f = 0.0f;
		static int counter = 0;
		
		ImGui::Begin("Hello, world!");                          // Create a window called "Hello, world!" and append into it.
		
		ImGui::Text("This is some useful text.");               // Display some text (you can use a format strings too)
		//ImGui::Checkbox("Demo Window", &show_demo_window);      // Edit bools storing our window open/close state
		//ImGui::Checkbox("Another Window", &show_another_window);
		
		ImGui::SliderFloat("float", &f, 0.0f, 1.0f);            // Edit 1 float using a slider from 0.0f to 1.0f
		ImGui::ColorEdit3("clear color", (float*)&color); // Edit 3 floats representing a color
		
		if (ImGui::Button("Button"))                            // Buttons return true when clicked (most widgets return true when edited/activated)
			counter++;
		ImGui::SameLine();
		ImGui::Text("counter = %d", counter);
		
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
		ImGui::End();
	}
	
	// Rendering
	ImGui::Render();
	ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
	// Update and Render additional Platform Windows
	// (Platform functions may change the current OpenGL context, so we save/restore it to make it easier to paste this code elsewhere.
	//  For this specific demo app we could also call glfwMakeContextCurrent(window) directly)
	if (ImGui::GetIO().ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
	{
		GLFWwindow* backup_current_context = glfwGetCurrentContext();
		ImGui::UpdatePlatformWindows();
		ImGui::RenderPlatformWindowsDefault();
		glfwMakeContextCurrent(backup_current_context);
	}
}

void Window::initGLEW() {
	glewExperimental = GL_TRUE;
	if (glewInit() != GLEW_OK) {
		LOG_MODULE(LOG_ERROR, MODULE_NAME) << "Failed to initialize GLEW" << LOG_END;
	}
	
	if (glDebugEnabled) {
		glEnable(GL_DEBUG_OUTPUT);
		glDebugMessageCallback(glDebugCallback, nullptr);
	}
	
	printDeviceParameters();
	
	// tell GL to only draw onto a pixel if the shape is closer to the viewer
	glEnable(GL_DEPTH_TEST); // enable depth-testing
	glDepthFunc(GL_LESS); // depth-testing interprets a smaller value as "closer"
	glPolygonMode(GL_FRONT, GL_FILL);
	glPolygonMode(GL_FRONT, GL_FILL);
	glEnable(GL_BLEND);
	glEnable(GL_MULTISAMPLE);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}
