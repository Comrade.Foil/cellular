#ifndef CELLULAR_APPCONFIG_H
#define CELLULAR_APPCONFIG_H

#include <glm/vec3.hpp>
#include <glm/vec2.hpp>
#include <glm/vec4.hpp>
#include <string>

struct AppConfig {
	bool isGLDebug = false;

	unsigned int windowWidth = 896;
	unsigned int windowHeight = 896;

	unsigned int fov = 45;
	bool isFullscreen = false;
	glm::vec2 worldSize = glm::vec2(32., 32.);
	glm::vec3 cameraPosition = glm::vec3(16., 16., 32.);
	glm::vec4 backgroundColor = glm::vec4(0., 0., 0., 1.);
};

#endif //CELLULAR_APPCONFIG_H
