#ifndef CELLULAR_LOGGER_H
#define CELLULAR_LOGGER_H

#include "string"
#include "iostream"

#define LOG(level, msg) level << msg << LOG_END

#define LOG_MODULE(level, module) level << "(" + module + ")"

#define LOG_ERROR std::cout << "[ERROR]:    "

#define LOG_WARNING std::cout << "[WARNING]:  "

#define LOG_INFO std::cout << "[INFO]:     "

#define LOG_END std::endl

#define LOG_NEWLINE "\n"


#endif //CELLULAR_LOGGER_H
