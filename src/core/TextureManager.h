#ifndef CELLULAR_TEXTUREMANAGER_H
#define CELLULAR_TEXTUREMANAGER_H

#include "unordered_map"
#include "vector"
#include "string"
#include "Logger.h"
#include "GLEW/glew.h"
#include "stb/stb_image.h"
#include "NonCopyable.h"
#include "../graphics/Texture.h"

#define GET_TEXTURE(name) TextureManager::getTexture(name)
#define GET_CUBEMAP(name) TextureManager::getCubeMap(name)

using namespace std;

class TextureManager : public NonCopyable {
private:
	TextureManager() = default;

	static const string MODULE_NAME;
	static std::unordered_map<string, Texture*> textures;
	static std::unordered_map<string, unsigned int> cubeMaps;

public:
	static void loadTexture(const string& path, const string& name);
	static void loadCubeMap(vector<string>& , string name);
	
	static void storeTexture(Texture* texture, const string& name);

	static Texture* getTexture(const string& name);
	static unsigned int getCubeMap(const string& name);

	static void free();

};


#endif //CELLULAR_TEXTUREMANAGER_H
