#ifndef CELLULAR_MODELSMANAGER_H
#define CELLULAR_MODELSMANAGER_H

#include "unordered_map"
#include "vector"
#include "string"

#include "NonCopyable.h"
#include "../MeshData.h"

using namespace std;

#define GET_MODEL(name) ModelsManager::getModel(name)

class ModelsManager : public NonCopyable {
private:
    ModelsManager() = default;

    static const string MODULE_NAME;
    static unordered_map<string, MeshData*> models;
    static const string MODELS_ROOT_PATH;

    static MeshData* loadObj(const string &fileName);

public:
    static void loadModel(const string& relativePath, const string& modelName);
    static MeshData* getModel(const string& name);
    static void free();

};


#endif //CELLULAR_MODELSMANAGER_H
