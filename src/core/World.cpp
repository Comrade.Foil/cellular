#include "World.h"

#include "../systems/SPlayerControls.h"

#include "../systems/SBatchedDebugLineRenderer.h"


World::World(const AppConfig &config) {
	this->size = config.worldSize;
}

World::~World() {
	entities.clear();
}

const glm::vec2 &World::getSize() const {
	return size;
}

void World::addEntity(shared_ptr<Entity> en) {
    this->entities.insert(make_pair(en->getId(), en));
}

void World::update(entt::registry& reg, const Window &window, const Camera &camera) {
	SBatchedDebugLineRenderer::update(reg, window, camera);
	
	for (auto& item : entities) {
        SPlayerControls::update(window, *this);

		item.second->update(window, *this);
		item.second->draw(window, camera);
	}
}

shared_ptr<Entity> World::getEntity(unsigned int id) {
    if (entities.find(id) == entities.end()) {
        LOG_ERROR << "couldn't find entity " << LOG_END;
        return nullptr;
    } else {
        return entities[id];
    }
}
