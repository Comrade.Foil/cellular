#ifndef CELLULAR_WORLD_H
#define CELLULAR_WORLD_H

#include <unordered_map>
#include <memory>
#include "AppConfig.h"
#include "glm/vec3.hpp"
#include "glm/vec4.hpp"
#include "../Window.h"
#include "../Camera.h"
#include "Entity.h"
#include "NonCopyable.h"
#include <entt/entity/registry.hpp>


#define ALOT 10000.0

using namespace glm;

class World : public NonCopyable {
private:
	vec2 size;
	std::unordered_map<unsigned int, shared_ptr<Entity>> entities;
public:
	explicit World(const AppConfig& config);
	~World();
	
	void addEntity(shared_ptr<Entity> en);
	shared_ptr<Entity> getEntity(unsigned int id);
	const glm::vec2 &getSize() const;
	int getEntitiesSize() { return this->entities.size(); };
	void update(entt::registry& reg, const Window& window, const Camera& camera);
	void clear();
};


#endif //CELLULAR_WORLD_H
