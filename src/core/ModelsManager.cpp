#define TINYOBJLOADER_IMPLEMENTATION

#include "iostream"
#include <tiny_obj_loader.h>

#include "ModelsManager.h"
#include "Logger.h"


const string ModelsManager::MODULE_NAME = "ModelsManager";
const string ModelsManager::MODELS_ROOT_PATH = "resources//models//";
unordered_map<string, MeshData*> ModelsManager::models = unordered_map<string, MeshData*>();

void ModelsManager::loadModel(const string &relativePath, const string& modelName) {
    MeshData* meshData = loadObj(relativePath);
    models.insert(make_pair(modelName, meshData));
}

void ModelsManager::free() {
    for (const auto& item : models) {
        delete item.second;
    }
    models.clear();
}

// Only loads vertices data
MeshData* ModelsManager::loadObj(const string& fileName) {
    std::vector<tinyobj::material_t> materials;
    std::string warn;
    std::string err;
    tinyobj::attrib_t attrib;
    std::vector<tinyobj::shape_t> shapes;

    bool ret = tinyobj::LoadObj(&attrib, &shapes, &materials, &err, fileName.c_str(), MODELS_ROOT_PATH.c_str());
    if (!ret) {
		LOG_MODULE(LOG_ERROR, MODULE_NAME) << "Couldn't fetch model data: " + fileName << LOG_END;
    }
    std::vector<float> vertices;
    auto* meshData = new MeshData();

    for (int i = 0; i < shapes[0].mesh.indices.size(); i++) {
        int xIndex = shapes[0].mesh.indices[i].vertex_index * 3;
        int yIndex = shapes[0].mesh.indices[i].vertex_index * 3 + 1;
        int zIndex = shapes[0].mesh.indices[i].vertex_index * 3 + 2;

        meshData->vertices.push_back(attrib.vertices[xIndex]);
        meshData->vertices.push_back(attrib.vertices[yIndex]);
        meshData->vertices.push_back(attrib.vertices[zIndex]);
    }

    return meshData;
}

MeshData *ModelsManager::getModel(const string &name) {
    if (models.find(name) == models.end()) {
		LOG_MODULE(LOG_ERROR, MODULE_NAME) << "model not found in map: " + name << LOG_END;
        return nullptr;
    } else {
        return models[name];
    }
}


