#define STB_IMAGE_IMPLEMENTATION
#include "TextureManager.h"

std::unordered_map<string, Texture*> TextureManager::textures = unordered_map<string, Texture*>();
std::unordered_map<string, unsigned int> TextureManager::cubeMaps = unordered_map<string, unsigned int>();
const string TextureManager::MODULE_NAME = "TextureManager";

void TextureManager::free() {
	for (const auto& item : textures) {
		delete item.second;
	}
	for (const auto& item : cubeMaps) {
		glDeleteTextures(1, &item.second);
	}
	
	textures.clear();
	cubeMaps.clear();
}

void TextureManager::loadTexture(const string& path, const string& name) {
	stbi_set_flip_vertically_on_load(true);
	
	unsigned int textureId;
	int width, height, nrChannels;
	unsigned char* data = stbi_load(path.c_str(), &width, &height, &nrChannels, 0);
	
	if (data != nullptr) {
		GLenum format;
		if (nrChannels == 1)
			format = GL_RED;
		else if (nrChannels == 3)
			format = GL_RGB;
		else if (nrChannels == 4)
			format = GL_RGBA;
		
		glGenTextures(1, &textureId);
		glBindTexture(GL_TEXTURE_2D, textureId);
		glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
		glGenerateMipmap(GL_TEXTURE_2D);
		
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		
		stbi_image_free(data);
		
		Texture* textureDescriptor = new Texture();
		textureDescriptor->id = textureId;
		textureDescriptor->format = format;
		textureDescriptor->width = width;
		textureDescriptor->height = height;
		textureDescriptor->type = GL_UNSIGNED_BYTE;
		
		storeTexture(textureDescriptor, name);
		glBindTexture(GL_TEXTURE_2D, 0);
		
	} else {
		LOG_MODULE(LOG_ERROR, MODULE_NAME) << "Couldn't load texture " + path << LOG_END;
		stbi_image_free(data);
	}
}

void TextureManager::loadCubeMap(vector<string> &faces, string name) {
	stbi_set_flip_vertically_on_load(true);
	
	unsigned int textureId;
	glGenTextures(1, &textureId);
	glBindTexture(GL_TEXTURE_CUBE_MAP, textureId);
	
	int width, height, nrChannels;
	for (unsigned int i = 0; i < faces.size(); i++)
	{
		unsigned char *data = stbi_load(faces[i].c_str(), &width, &height, &nrChannels, 0);
		if (data)
		{
			glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
						 0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data
			);
			stbi_image_free(data);
		}
		else
		{
			LOG_MODULE(LOG_ERROR, MODULE_NAME) << "Cubemap texture failed to load at path: " + faces[i] << LOG_END;
			stbi_image_free(data);
			glDeleteTextures(1, &textureId);
			return;
		}
	}
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
	
	cubeMaps.insert(make_pair(name, textureId));
	
	glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
}

Texture* TextureManager::getTexture(const string& name) {
	if (textures.find(name) == textures.end()) {
		LOG_MODULE(LOG_ERROR, MODULE_NAME) << "Texture is not found: " + name << LOG_END;
		return NULL;
	} else {
		return textures[name];
	}
}

unsigned int TextureManager::getCubeMap(const string& name) {
	if (cubeMaps.find(name) == cubeMaps.end()) {
		LOG_MODULE(LOG_ERROR, MODULE_NAME) << "Cubemap texture is not found: " + name << LOG_END;
		return NULL;
	} else {
		return cubeMaps[name];
	}
}

void TextureManager::storeTexture(Texture *texture, const string& name) {
	textures.insert(make_pair(name, texture));
}
