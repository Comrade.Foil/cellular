#ifndef CELLULAR_NONCOPYABLE_H
#define CELLULAR_NONCOPYABLE_H

class NonCopyable {
public:
	NonCopyable() = default;
	NonCopyable(const NonCopyable &) = delete;
	NonCopyable &operator=(const NonCopyable &) = delete;
};

#endif //CELLULAR_NONCOPYABLE_H
