#ifndef CELLULAR_SHADERMANAGER_H
#define CELLULAR_SHADERMANAGER_H

#include "unordered_map"
#include "../graphics/Shader.h"
#include "NonCopyable.h"

#define GET_SHADER(name) ShaderManager::getShader(name)

class ShaderManager : public NonCopyable {
private:
	ShaderManager() = default;

	static const string MODULE_NAME;
	static std::unordered_map<string, Shader*> shaders;
public:
	static void addShader(Shader* shader);
	static Shader* getShader(const string& shaderName);

	static void free();
};


#endif //CELLULAR_SHADERMANAGER_H
