#ifndef CELLULAR_UTILS_H
#define CELLULAR_UTILS_H

#include <memory>

#define BIT(x) (1 << x)

template<typename T>
using Ref = std::shared_ptr<T>;

template<typename T, typename ... Args>
constexpr Ref<T> CreateRef(Args &&... args) {
	return std::make_shared<T>(std::forward<Args>(args)...);
}
#endif //CELLULAR_UTILS_H
