#ifndef CELLULAR_ENTITY_H
#define CELLULAR_ENTITY_H

#include "glm/vec4.hpp"
#include "../Window.h"
#include "../Camera.h"
//#include "World.h"

using namespace glm;

class World;

class Entity {
private:
	static unsigned int newId() {
		static unsigned int n_Id = 0;
		return n_Id++;
	}

protected:
	unsigned int id;
	vec3 acceleration = vec3(0.0);
	vec3 position = vec3(0.0);
	vec3 velocity = vec3(0.0);

public:
    Entity() {
    	id = newId();
    };

    virtual ~Entity(){};

    virtual void update(const Window& window, const World& world) = 0;
    virtual void draw(const Window& window, const Camera& camera) = 0;

	const glm::vec3 &getPosition() const {
		return position;
	}

	const glm::vec3 &getVelocity() const {
		return velocity;
	}

	void setPosition(const vec3 &position) {
		this->position = position;
	}

	void setVelocity(const vec3 &velocity) {
		this->velocity = velocity;
	}
	
	const vec3 &getAcceleration() const {
		return acceleration;
	}
	
	void setAcceleration(const vec3 &acceleration) {
		this->acceleration = acceleration;
	}
	
	void addAcceleration(const vec3 &acceleration) {
		this->acceleration += acceleration;
	}

	unsigned int getId() const {
		return id;
	}
};



#endif //CELLULAR_ENTITY_H
