#include "ShaderManager.h"
#include "Logger.h"

std::unordered_map<string, Shader*> ShaderManager::shaders = std::unordered_map<string, Shader*>();
const string ShaderManager::MODULE_NAME = "ShaderManager";

Shader *ShaderManager::getShader(const string& shaderName) {
	if (shaders.find(shaderName) == shaders.end()) {
		LOG_MODULE(LOG_ERROR, MODULE_NAME) << "program program is not found: " + shaderName << LOG_END;
		return nullptr;
	} else {
		return shaders[shaderName];
	}

}

void ShaderManager::addShader(Shader *shader) {
	shaders.insert(make_pair(shader->getName(), shader));
}

void ShaderManager::free() {
	for (const auto& item : shaders) {
		delete item.second;
	}
	shaders.clear();
}
