#include "InstancedLines.h"

#include "../core/ShaderManager.h"
#include "glm/gtc/random.hpp"

vector<vec3> initializeGeometry() {
	const float PI = pi<float>();
	
	vector<vec3> instanceGeometry = {
		vec3(0, -0.5, 0), vec3(0, -0.5, 1), vec3(0, 0.5, 1),
		vec3(0, -0.5, 0), vec3(0, 0.5, 1), vec3(0, 0.5, 0)
	};
	
	// Draw front cap
	for (int step = 0; step < LINES_CAP_RESOLUTION; step++) {
		const float theta0 = PI / 2 + ((float)(step + 0) * PI) / LINES_CAP_RESOLUTION;
		const float theta1 = PI / 2 + ((float)(step + 1) * PI) / LINES_CAP_RESOLUTION;
		
		instanceGeometry.push_back(vec3(0, 0, 0));
		instanceGeometry.push_back(vec3(0.5 * cos(theta0),0.5 * sin(theta0),  0.0));
		instanceGeometry.push_back(vec3(0.5 * cos(theta1),0.5 * sin(theta1), 0.0));
	}
	
	// Draw back cap
	for (int step = 0; step < LINES_CAP_RESOLUTION; step++) {
		const float theta0 = (3 * PI) / 2 + ((float)(step + 0) * PI) / LINES_CAP_RESOLUTION;
		const float theta1 = (3 * PI) / 2 + ((float)(step + 1) * PI) / LINES_CAP_RESOLUTION;
		
		instanceGeometry.push_back(vec3(0, 0, 1));
		instanceGeometry.push_back(vec3(0.5 * cos(theta0),0.5 * sin(theta0), 1));
		instanceGeometry.push_back(vec3(0.5 * cos(theta1),0.5 * sin(theta1), 1));
	}

	return instanceGeometry;
}

const vector<vec3> InstancedLines::geometryVertices = initializeGeometry();
const string InstancedLines::MODULE_NAME = "InstancedLines";


void InstancedLines::setPoints(const vector<vec2>& p) {
	points = p;
}

void InstancedLines::setLineWidth(const GLfloat& width) {
	lineWidth = width;
}

InstancedLines::InstancedLines(const vector<vec2>& p, GLfloat width, DRAW_MODE mode) {
	shader = ShaderManager::getShader("instancedLines");
	setPoints(p);
	setLineWidth(width);
	setDrawMode(mode);
	initColors();
	initBuffers();
}

void InstancedLines::render(const Window& window, const Camera& camera) {
	updateTransforms();

    shader->use();
	shader->setUniformMatrix4fv("uTransform", getTransformMat());
	shader->setUniformMatrix4fv("uMVP", camera.getMVP());
	shader->setUniform1f("uWidth", lineWidth);
	glBindVertexArray(vao);
	glDrawArraysInstanced(GL_TRIANGLES, 0, geometryVertices.size(), instancesCount);
	glBindVertexArray(0);
}

void InstancedLines::setDrawMode(const DRAW_MODE& mode) {
	drawMode = mode;
	determineDrawSettings();
}

void InstancedLines::determineDrawSettings() {
	if (drawMode == DRAW_MODE::LINES) {
		stride = 2*sizeof(vec2);
		instancesCount = points.size() / 2;
	} else if (drawMode == DRAW_MODE::STRIP) {
		stride = sizeof(vec2);
		instancesCount = points.size() - 1;
	} else {
		LOG_MODULE(LOG_ERROR, MODULE_NAME) << "Wrong draw mode specified" << LOG_END;
	}
}

InstancedLines::InstancedLines() {

}

void InstancedLines::initBuffers() {
	geometryBuffer = getGeometryBuffer();
	pointsBuffer.allocate(points, GL_DYNAMIC_DRAW);
	colorsBuffer.allocate(colors, GL_STATIC_DRAW);

	glCreateVertexArrays(1, &vao);
	// creating binding points 0, 1 and 2 of buffers to vertex array object (VAO)
	glVertexArrayVertexBuffer(vao, 0, geometryBuffer.getId(), 0, sizeof(vec3));
	glVertexArrayVertexBuffer(vao, 1, pointsBuffer.getId(), 0, stride);
	glVertexArrayVertexBuffer(vao, 2, colorsBuffer.getId(), 0, sizeof(vec4));
	// modify the rate at which generic vertex attributes advance per instance (binding 1 or 2, advance 1)
	glVertexArrayBindingDivisor(vao, 1, 1);
	glVertexArrayBindingDivisor(vao, 2, 1);
	// enable generic vertex attribute arrays
	glEnableVertexArrayAttrib(vao, 0);
	glEnableVertexArrayAttrib(vao, 1);
	glEnableVertexArrayAttrib(vao, 2);
	glEnableVertexArrayAttrib(vao, 3);
	glEnableVertexArrayAttrib(vao, 4);
	// specify the organization of vertex arrays (similar to glVertexAttribPointer, must work with glVertexArrayVertexBuffer)
	glVertexArrayAttribFormat(vao, 0, 3, GL_FLOAT, GL_FALSE, 0);
	glVertexArrayAttribFormat(vao, 1, 2, GL_FLOAT, GL_FALSE, 0);
	glVertexArrayAttribFormat(vao, 2, 2, GL_FLOAT, GL_FALSE, sizeof(vec2));
	glVertexArrayAttribFormat(vao, 3, 4, GL_FLOAT, GL_FALSE, 0);
	glVertexArrayAttribFormat(vao, 4, 4, GL_FLOAT, GL_FALSE, sizeof(vec4));
	// associate a vertex attribute and a vertex buffer binding for a vertex array object
	glVertexArrayAttribBinding(vao, 0, 0);
	glVertexArrayAttribBinding(vao, 1, 1);
	glVertexArrayAttribBinding(vao, 2, 1);
	glVertexArrayAttribBinding(vao, 3, 2);
	glVertexArrayAttribBinding(vao, 4, 2);
	glBindVertexArray(0);

// old-fashioned way without DSA:
//	glGenVertexArrays(1, &vao);
//	glBindVertexArray(vao);
//	geometryBuffer.bind();
//	glEnableVertexAttribArray(0);
//	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), (void *)0);
//	geometryBuffer.unbind();
//	pointsBuffer.allocate(points, GL_DYNAMIC_DRAW);
//	pointsBuffer.bind();
//	glEnableVertexAttribArray(1);
//	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, stride, (void*) 0);
//	glVertexAttribDivisor(1, 1);
//	glEnableVertexAttribArray(2);
//	glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, stride, (void*)(sizeof(vec2)));
//	glVertexAttribDivisor(2, 1);
//	pointsBuffer.unbind();
//	colorsBuffer.allocate(colors, GL_STATIC_DRAW);
//	colorsBuffer.bind();
//	glEnableVertexAttribArray(3);
//	glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(vec4), (void*)0);
//	glVertexAttribDivisor(3, 1);
//	glEnableVertexAttribArray(4);
//	glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(vec4), (void*)(sizeof(vec4)));
//	glVertexAttribDivisor(4, 1);
//	colorsBuffer.unbind();
}

const BufferObject &InstancedLines::getPointsBuffer() const {
	return pointsBuffer;
}

unsigned int InstancedLines::getPointsBufferSize() const {
	return points.size();
}

void InstancedLines::initColors() {
	colors.clear();
	colors.shrink_to_fit();
	for (int i = 0; i < points.size() - 1; i++) {
		colors.push_back(vec4(glm::linearRand(0.0, 1.0), glm::linearRand(0.0, 1.0), glm::linearRand(0.0, 1.0), 1.0));
	}
	colors.push_back(colors[0]);
}

BufferObject& InstancedLines::getGeometryBuffer() {
	static BufferObject buff(geometryVertices, GL_STATIC_DRAW);
	return buff;
}
