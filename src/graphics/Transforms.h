#ifndef CELLULAR_TRANSFORMS_H
#define CELLULAR_TRANSFORMS_H

#include "glm/vec4.hpp"
#include "glm/vec2.hpp"
#include "glm/vec3.hpp"
#include "glm/mat4x4.hpp"
#include "glm/gtx/quaternion.hpp"

using namespace glm;

class Transforms {
private:
    vec3 position;
    vec3 rotationAxis;
    float rotationAngle;
    mat4 rotationMat;
    mat4 scaleMat;
    mat4 transformMat;

    void updateRotationMat();

public:
    Transforms();

    void setPosition(const vec2 &pos);
    void setPosition(const vec3 &pos);
    void setPosition(float x, float y, float z);
    void setPosition(float x, float y);

    void translate(const vec2& offset);
    void translate(const vec3& offset);

    void setScale(float scale);
    void setScale(float scaleX, float scaleY, float scaleZ);
    void setScale(const vec3& scale);

    void setRotation(float angle, float x, float y, float z);
    void setRotation(float angle, const vec3& axis);
    void setRotationAngle(float angle);

    const vec3& getPosition() const;
    const mat4& getTransformMat() const;

    void update();
};

#endif //CELLULAR_TRANSFORMS_H
