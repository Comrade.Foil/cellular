#ifndef CELLULAR_LINE_H
#define CELLULAR_LINE_H

#include "glm/vec3.hpp"
#include "Shader.h"
#include "BufferObject.h"
#include "../Window.h"
#include "../Camera.h"
#include "DisplayObject.h"

using namespace glm;

class Line : public DisplayObject {
public:
	GLuint vao;
	vector<vec3> vertices;
	vec4 color;
	BufferObject vertexBuffer;
	BufferObject colorBuffer;

	Shader* shader;

	Line(vec3 p1, vec3 p2, vec4 color, Shader* shader);
	void render(const Window& window, const Camera& camera) override;
};


#endif //CELLULAR_LINE_H
