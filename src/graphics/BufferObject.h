#ifndef BUFFER_OBJECT_H
#define BUFFER_OBJECT_H

#include <GLEW/glew.h>
#include <glm/glm.hpp>
#include <memory>
#include <vector>

class BufferObject {
public:
	BufferObject() = default;

	template<typename T>
	BufferObject(const std::vector<T> &vector, GLenum usage) {
		allocate(vector, usage);
		unbind();
	};

    void create();
    void create(GLenum target);
    void setData(GLsizeiptr sizeBytes, const void *dataPtr, GLenum usage);
    void allocate(GLsizeiptr sizeBytes, GLenum usage);
    void allocate(GLsizeiptr sizeBytes, const void *data, GLenum usage);
    void reallocate(GLsizeiptr sizeBytes, const void *data, GLenum usage);
	void allocate(GLuint target, GLsizeiptr sizeBytes, const void *data, GLenum usage);

    template<typename T>
    void allocate(const std::vector<T> &vector, GLenum usage) {
        allocate(vector.size() * sizeof(T), &vector[0], usage);
    };
	
	template<typename T>
	void reallocate(const std::vector<T> &vector, GLenum usage) {
		reallocate(vector.size() * sizeof(T), &vector[0], usage);
	};

	template<typename T>
	void allocate(GLenum target, const std::vector<T> &vector, GLenum usage) {
		allocate(target, vector.size() * sizeof(T), &vector[0], usage);
	};

	void* mapData(GLenum access) const;
	template<typename T>
	T* mapData(GLenum access) {
		return static_cast<T*>(mapData(access));
	}

	void* mapRange(GLintptr offset, GLsizeiptr length, GLenum access) const;
	template<typename T>
	T* mapRange(GLintptr offset, GLsizeiptr length, GLenum access) const {
		return static_cast<T*>(mapRange(offset, length, access));
	}

	void updateData(GLintptr offset, GLsizeiptr sizeBytes, const void *data);


	void bind(GLenum target) const;
    void unbind(GLenum target) const;
	void bind() const;
	void unbind() const;
    void bindBase(GLenum target, GLuint index) const;
    void unbindBase(GLenum target, GLuint index) const;
	void bindBase(GLuint index) const;
	void unbindBase(GLuint index) const;

    bool isCreated() const;
    GLuint getId() const;
    
    GLsizeiptr getDataSize();

private:
    struct Data {
        Data();
        ~Data();
        GLuint id;
        GLsizeiptr size;
        GLenum lastTarget;
        bool isBound;
    };
    std::shared_ptr<Data> data;
};


#endif //BUFFER_OBJECT_H
