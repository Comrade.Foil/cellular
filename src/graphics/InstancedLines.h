#ifndef CELLULAR_INSTANCEDLINES_H
#define CELLULAR_INSTANCEDLINES_H

#include <GLEW/glew.h>
#include "glm/vec2.hpp"
#include "vector"
#include "DisplayObject.h"
#include "BufferObject.h"
#include "Shader.h"

using namespace glm;
using namespace std;

#define LINES_CAP_RESOLUTION 16

class InstancedLines : public DisplayObject {
public:
	enum DRAW_MODE {
		LINES, STRIP
	};

private:
	const static string MODULE_NAME;
	const static vector<vec3> geometryVertices;
	static BufferObject& getGeometryBuffer();

	BufferObject geometryBuffer;


	Shader* shader;

	DRAW_MODE drawMode;
	vector<vec2> points;
	vector<vec4> colors;
	BufferObject pointsBuffer;
	BufferObject colorsBuffer;
	GLfloat lineWidth;
	GLuint stride;
	GLuint instancesCount;

	void determineDrawSettings();
	void initColors();
	void initBuffers();

public:
	GLuint vao;

	InstancedLines(const vector<vec2>& p, GLfloat width, DRAW_MODE drawMode);
	InstancedLines();

	void setPoints(const vector<vec2>& p);
	void setLineWidth(const GLfloat& width);
	void setDrawMode(const DRAW_MODE& drawMode);

	unsigned int getPointsBufferSize() const;
	const BufferObject &getPointsBuffer() const;

	void render(const Window& window, const Camera& camera) override;
};


#endif //CELLULAR_INSTANCEDLINES_H
