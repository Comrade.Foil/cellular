#include "BufferObject.h"

BufferObject::Data::Data() : id(0), size(0), lastTarget(GL_ARRAY_BUFFER) {
    glCreateBuffers(1, &id);
}

BufferObject::Data::~Data() {
    glDeleteBuffers(1, &id);
}


void BufferObject::create() {
    data = std::make_shared<Data>();
}

void BufferObject::create(GLenum target) {
    data = std::make_shared<Data>();
    data->lastTarget = target;
}

void BufferObject::setData(GLsizeiptr sizeBytes, const void *dataPtr, GLenum usage) {
    if (!data) return;

    data->size = sizeBytes;
    bind(data->lastTarget);
    glBufferData(data->lastTarget, sizeBytes, dataPtr, usage);
}

void BufferObject::bind(GLenum target) const {
    if (!data) return;

    glBindBuffer(target, data->id);
    data->lastTarget = target;
    data->isBound = true;
}

void BufferObject::unbind(GLenum target) const {
    if (!data) return;

    glBindBuffer(target, 0);
    data->isBound = false;
}

void BufferObject::bind() const {
	if (!data) return;

	glBindBuffer(data->lastTarget, data->id);
	data->isBound = true;
}

void BufferObject::unbind() const {
	if (!data) return;

	glBindBuffer(data->lastTarget, 0);
	data->isBound = false;
}

void BufferObject::bindBase(GLenum target, GLuint index) const {
	if (!this->data) return;

	glBindBufferBase(target, index, data->id);
	data->lastTarget = target;
	data->isBound = true;
}

void BufferObject::unbindBase(GLenum target, GLuint index) const {
	if (!this->data) return;

	glBindBufferBase(target, index, 0);
	data->lastTarget = target;
	data->isBound = false;
}

void BufferObject::bindBase(GLuint index) const {
	if (!this->data) return;

	glBindBufferBase(data->lastTarget, index, data->id);
	data->isBound = true;
}

void BufferObject::unbindBase(GLuint index) const {
	if (!this->data) return;

	glBindBufferBase(data->lastTarget, index, 0);
	data->isBound = false;
}
bool BufferObject::isCreated() const {
    return data != nullptr;
}

GLuint BufferObject::getId() const {
    if (!data) return 0;
    return data->id;
}

void *BufferObject::mapData(GLenum access) const {
    if (!data) return nullptr;

    bind();
    void *p = glMapBufferRange(data->lastTarget, 0, data->size, access);
    glUnmapBuffer(data->lastTarget);
    unbind();

    return p;
}

void BufferObject::allocate(GLsizeiptr sizeBytes, GLenum usage) {
    create();
    setData(sizeBytes, nullptr, usage);
}

void BufferObject::allocate(GLsizeiptr sizeBytes, const void *data, GLenum usage) {
    create();
    setData(sizeBytes, data, usage);
}

void BufferObject::reallocate(GLsizeiptr sizeBytes, const void *data, GLenum usage) {
	setData(sizeBytes, data, usage);
}

void BufferObject::updateData(GLintptr offset, GLsizeiptr sizeBytes, const void *data) {
	if (!this->data) return;

	bind(this->data->lastTarget);
	glBufferSubData(this->data->lastTarget, offset, sizeBytes, data);
	unbind(this->data->lastTarget);
}

void BufferObject::allocate(GLuint target, GLsizeiptr sizeBytes, const void *data, GLenum usage) {
	create(target);
	setData(sizeBytes, data, usage);
}

GLsizeiptr BufferObject::getDataSize() {
	if (!this->data) return 0;
	
	return data->size;
}

void *BufferObject::mapRange(GLintptr offset, GLsizeiptr length, GLenum access) const {
	if (!data) return nullptr;

	bind();
	void *p = glMapBufferRange(data->lastTarget, offset, length, access);
	glUnmapBuffer(data->lastTarget);
	unbind();

	return p;
}


//template<>
//void BufferObject::allocate<float>(const std::vector<float> &vector, GLenum usage) {
//    allocate(vector.size() * sizeof(float), &vector[0], usage);
//}
//
//template<>
//void BufferObject::allocate<glm::ivec2>(const std::vector<glm::ivec2> &vector, GLenum usage) {
//    allocate(vector.size() * sizeof(float), &vector[0], usage);
//}
