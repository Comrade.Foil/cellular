#include "Line.h"

Line::Line(vec3 p1, vec3 p2, vec4 color, Shader* shader) : color(color)
{
	this->shader = shader;
	vertices.push_back(p1);
	vertices.push_back(p2);
	
	glGenVertexArrays(1, &vao);
	glBindVertexArray(vao);
	
	vertexBuffer.allocate(vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), (void *) 0);
	vertexBuffer.unbind();
	glBindVertexArray(0);
}

void Line::render(const Window& window, const Camera& camera) {
	glUseProgram(shader->getProgramId());
	glBindVertexArray(vao);
	vertexBuffer.bind();
	glEnableVertexAttribArray(0);
	shader->setUniform4f("uColor", color);
	shader->setUniformMatrix4fv("uTransform", getTransformMat());
	shader->setUniformMatrix4fv("uMVP", camera.getMVP());
	
	glDrawArrays(GL_LINES, 0, 2);
	glUseProgram(0);
	vertexBuffer.unbind();
}
