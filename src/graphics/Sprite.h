#ifndef CELLULAR_SPRITE_H
#define CELLULAR_SPRITE_H

#include <vector>
#include <utility>

#include "DisplayObject.h"
#include "BufferObject.h"
#include "Shader.h"
#include "Transforms.h"
#include "Texture.h"

class Sprite : public DisplayObject {
private:
	const static vector<vec2> textureCoordinates;
	const static vector<vec4> geometryVertices;
	static BufferObject& getGeometryBuffer();
	static BufferObject& getTextureCoordsBuffer();

	BufferObject geometryBuffer;
	BufferObject textureCoordsBuffer;

	Texture* texture;
	Shader* program;

	void initBuffers();

public:
	GLuint vao;

	float alpha;
	void render(const Window& window, const Camera& camera) override;
	Sprite(Texture* texture, Shader* shader);
};


#endif //CELLULAR_SPRITE_H
