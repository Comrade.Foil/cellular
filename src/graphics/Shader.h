#ifndef FIESTA_SHADER_H
#define FIESTA_SHADER_H

#include <string>
#include <unordered_map>

#include <GLEW/glew.h>
#include "glm/vec2.hpp"
#include "glm/vec3.hpp"
#include "glm/vec4.hpp"
#include <glm/detail/type_mat4x4.hpp>
#include <glm/gtc/type_ptr.hpp>

using namespace std;
using namespace glm;

class Shader {
public:
	static const string MODULE_NAME;
	

    Shader(string name, string vShaderPath, string fShaderPath, string gShaderPath);
	Shader(string name, string vShaderPath, string fShaderPath);
	Shader(string name, string computeShaderPath);
    ~Shader();

    void create(string name, string vShaderSource, string fShaderSource, string gShaderSource);
    void create(string name, string vShaderSource, string fShaderSource);
    void create(string name, string computeShaderPath);

	void setUniform1f(const string &uniformName, float f1) const;
	void setUniform1i(const string &uniformName, int f1) const;
	void setUniform2f(const string &uniformName, float f1, float f2) const;
	void setUniform2i(const string &uniformName, int i1, int i2) const;
	void setUniform3i(const string &uniformName, int i1, int i2, int i3) const;
	void setUniform3f(const string &uniformName, float f1, float f2, float f3) const;
	void setUniform4f(const string &uniformName, float f1, float f2, float f3, float f4) const;

	void setUniform2i(const string &uniformName, const ivec2& vec) const;
	void setUniform3i(const string &uniformName, const ivec3& vec) const;
	void setUniform3f(const string &uniformName, const fvec3& vec) const;
	void setUniform4f(const string &uniformName, const fvec4& vec) const;
	void setUniformMatrix4fv(const string &uniformName, const mat4& matrix) const;

    void use() const;

	GLuint getProgramId() const;
	string getName() const;

private:
	GLuint programId;
	string name;
	mutable unordered_map<string, GLint> uniformsLocationCache;

	GLuint createProgram(GLuint &computeId);
    GLuint createProgram(GLuint &vertexId, GLuint &fragmentId);
    GLuint createProgram(GLuint &vertexId, GLuint &fragmentId, GLuint &geometryId);

    static string loadShader(const string & shaderPath);
    GLint getUniformLocation(const string& name) const;
};


#endif //FIESTA_SHADER_H
