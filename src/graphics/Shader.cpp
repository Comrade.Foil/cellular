#include "Shader.h"

#include <fstream>
#include <sstream>
#include <iostream>

#include "../core/Logger.h"


const string Shader::MODULE_NAME = "Shader";

Shader::Shader(string name, string vShaderPath, string fShaderPath, string gShaderPath) {
	this->create(name, vShaderPath, fShaderPath, gShaderPath);
}

Shader::Shader(string name, string vShaderPath, string fShaderPath) {
	this->create(name, vShaderPath, fShaderPath);
}

Shader::Shader(string name, string computeShaderPath) {
	this->create(name, computeShaderPath);
}

void Shader::create(string name, string vShaderPath, string fShaderPath, string gShaderPath) {
    this->name = name;

    const char *vertexSource = loadShader(vShaderPath).c_str(),
               *fragmentSource = loadShader(fShaderPath).c_str(),
               *geometrySource = loadShader(gShaderPath).c_str();

    GLuint vertex, fragment, geometry;
    GLint success;
    GLchar infoLog[512];

    vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vertexSource, NULL);
    glCompileShader(vertex);

    glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
    if(!success)
    {
        glGetShaderInfoLog(vertex, 512, NULL, infoLog);
        LOG_MODULE(LOG_ERROR, MODULE_NAME)
			<< "Failed to compile vertex shader" << LOG_NEWLINE << infoLog << LOG_END;
    };

    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fragmentSource, NULL);
    glCompileShader(fragment);

    glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
    if(!success)
    {
        glGetShaderInfoLog(fragment, 512, NULL, infoLog);
		LOG_MODULE(LOG_ERROR, MODULE_NAME)
			<< "Failed to compile fragment shader" << LOG_NEWLINE << infoLog << LOG_END;
    };

    geometry = glCreateShader(GL_GEOMETRY_SHADER);
    glShaderSource(geometry, 1, &geometrySource, NULL);
    glCompileShader(geometry);

    glGetShaderiv(geometry, GL_COMPILE_STATUS, &success);
    if(!success)
    {
        glGetShaderInfoLog(geometry, 512, NULL, infoLog);
		LOG_MODULE(LOG_ERROR, MODULE_NAME)
			<< "Failed to compile geometry shader" << LOG_NEWLINE << infoLog << LOG_END;
    };

    this->programId = createProgram(vertex, fragment, geometry);

    if (this->programId == 0) {
		LOG_MODULE(LOG_ERROR, MODULE_NAME) << "Failed shader: " << name << LOG_END;
    }

    glDeleteShader(vertex);
    glDeleteShader(fragment);
    glDeleteShader(geometry);
}

void Shader::create(string name, string vShaderPath, string fShaderPath) {
    this->name = name;

    string vSource = loadShader(vShaderPath),
           fSource =  loadShader(fShaderPath);

    const char *vertexSource   = vSource.c_str(),
               *fragmentSource = fSource.c_str();

    GLuint vertex, fragment;
    GLint success;
    GLchar infoLog[512];

    vertex = glCreateShader(GL_VERTEX_SHADER);
    glShaderSource(vertex, 1, &vertexSource, NULL);
    glCompileShader(vertex);

    glGetShaderiv(vertex, GL_COMPILE_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(vertex, 512, NULL, infoLog);
		LOG_MODULE(LOG_ERROR, MODULE_NAME)
			<< "Failed to compile vertex shader" << LOG_NEWLINE << infoLog << LOG_END;

	};

    fragment = glCreateShader(GL_FRAGMENT_SHADER);
    glShaderSource(fragment, 1, &fragmentSource, NULL);
    glCompileShader(fragment);

    glGetShaderiv(fragment, GL_COMPILE_STATUS, &success);
    if(!success) {
        glGetShaderInfoLog(fragment, 512, NULL, infoLog);
		LOG_MODULE(LOG_ERROR, MODULE_NAME)
			<< "Failed to compile fragment shader" << LOG_NEWLINE << infoLog << LOG_END;

	};

    this->programId = createProgram(vertex, fragment);

    if (this->programId == 0) {
		LOG_MODULE(LOG_ERROR, MODULE_NAME) << "Failed shader: " << name << LOG_END;
    }

    glDeleteShader(vertex);
    glDeleteShader(fragment);
}


void Shader::create(string name, string computeShaderPath) {
	this->name = name;

	string cSource = loadShader(computeShaderPath);

	const char *computeSource = cSource.c_str();

	GLuint computeShader = glCreateShader(GL_COMPUTE_SHADER);
	GLint success;
	GLchar infoLog[512];

	glShaderSource(computeShader, 1, &computeSource, NULL);
	glCompileShader(computeShader);

	glGetShaderiv(computeShader, GL_COMPILE_STATUS, &success);
	if(!success) {
		glGetShaderInfoLog(computeShader, 512, NULL, infoLog);
		LOG_MODULE(LOG_ERROR, MODULE_NAME)
			<< "Failed to compile geometry shader" << LOG_NEWLINE << infoLog << LOG_END;
	};

	this->programId = createProgram(computeShader);

	glDeleteShader(computeShader);
}

GLuint Shader::createProgram(GLuint &vertex, GLuint &fragment, GLuint &geometry) {
    GLint success;
    GLchar infoLog[512];
    GLuint program;

    program = glCreateProgram();

    glAttachShader(program, vertex);
    glAttachShader(program, fragment);
    glAttachShader(program, geometry);
    glLinkProgram(program);

    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(program, 512, NULL, infoLog);
		LOG_MODULE(LOG_ERROR, MODULE_NAME)
			<< "Shader program linking failed" << LOG_NEWLINE << infoLog << LOG_END;
        return 0;
    }

    return program;
}

GLuint Shader::createProgram(GLuint &vertex, GLuint &fragment) {
    GLint success;
    GLchar infoLog[512];
    GLuint program;

    program = glCreateProgram();
    glAttachShader(program, vertex);
    glAttachShader(program, fragment);
    glLinkProgram(program);

    glGetProgramiv(program, GL_LINK_STATUS, &success);
    if (!success) {
        glGetProgramInfoLog(program, 512, NULL, infoLog);
		LOG_MODULE(LOG_ERROR, MODULE_NAME)
			<< "Shader program linking failed" << LOG_NEWLINE << infoLog << LOG_END;
		return 0;
    }

    return program;
}

GLuint Shader::createProgram(GLuint &compute) {
	GLint success;
	GLchar infoLog[512];
	GLuint program;

	program = glCreateProgram();
	glAttachShader(program, compute);
	glLinkProgram(program);

	glGetProgramiv(program, GL_LINK_STATUS, &success);
	if (!success) {
		glGetProgramInfoLog(program, 512, NULL, infoLog);
		LOG_MODULE(LOG_ERROR, MODULE_NAME)
			<< "Shader program linking failed" << LOG_NEWLINE << infoLog << LOG_END;
		return 0;
	}

	return program;
}

string Shader::loadShader(const string & shaderPath) {
    std::string shaderSource;
    std::ifstream shaderStream(shaderPath.c_str(), std::ios::in);

    if (shaderStream.is_open()) {
        std::string line = "";
        while (getline(shaderStream, line)) {
            shaderSource += "\n" + line;
        }
        shaderStream.close();
    } else {
		LOG_MODULE(LOG_ERROR, MODULE_NAME)
			<< "Cannot load shader src: " << shaderPath << LOG_END;
		return 0;
    }

    return shaderSource;
}

void Shader::setUniform1f(const string & uniformName, float f1) const {
	GLint uniformId = getUniformLocation(uniformName.c_str());
	glUniform1f(uniformId, f1);
}

void Shader::setUniform2f(const string & uniformName, float f1, float f2) const {
	GLint uniformId = getUniformLocation(uniformName.c_str());
	glUniform2f(uniformId, f1, f2);
}

void Shader::setUniform3f(const string & uniformName, float f1, float f2, float f3) const {
	GLint uniformId = getUniformLocation(uniformName.c_str());
	glUniform3f(uniformId, f1, f2, f3);
}

void Shader::setUniform2i(const string & uniformName, int i1, int i2) const {
	GLint uniformId = getUniformLocation(uniformName.c_str());
	glUniform2i(uniformId, i1, i2);
}

void Shader::setUniform3i(const string &uniformName, int i1, int i2, int i3) const {
	GLint uniformId = getUniformLocation(uniformName.c_str());
	glUniform3i(uniformId, i1, i2, i3);
}

void Shader::setUniformMatrix4fv(const string & uniformName, const mat4& matrix) const {
	GLint uniformId = getUniformLocation(uniformName.c_str());
	glUniformMatrix4fv(uniformId, 1, GL_FALSE, glm::value_ptr(matrix));
}

GLuint Shader::getProgramId() const {
	return programId;
}

string Shader::getName() const {
	return name;
}

Shader::~Shader() {
	glDeleteProgram(this->programId);
}

void Shader::setUniform1i(const string &uniformName, int i1) const {
	GLint uniformId = getUniformLocation(uniformName.c_str());
	glUniform1i(uniformId, i1);
}

void Shader::use() const {
	glUseProgram(programId);
}

void Shader::setUniform4f(const string &uniformName, float f1, float f2, float f3, float f4) const {
	GLint uniformId = getUniformLocation(uniformName.c_str());
	glUniform4f(uniformId, f1, f2, f3, f4);
}

void Shader::setUniform2i(const string &uniformName, const ivec2& vec) const {
	setUniform2i(uniformName, vec.x, vec.y);
}

void Shader::setUniform3i(const string &uniformName, const ivec3& vec) const {
	setUniform3i(uniformName, vec.x, vec.y, vec.z);
}

void Shader::setUniform3f(const string &uniformName, const fvec3& vec) const {
	setUniform3f(uniformName, vec.x, vec.y, vec.z);
}

void Shader::setUniform4f(const string &uniformName, const fvec4& vec) const {
	setUniform4f(uniformName, vec.x, vec.y, vec.z, vec.w);
}

GLint Shader::getUniformLocation(const string &name) const {
	if (uniformsLocationCache.find(name) != uniformsLocationCache.end()) {
		return uniformsLocationCache[name];
	}
	
	GLint location = glGetUniformLocation(programId, name.c_str());
	if (location == -1) {
		LOG_MODULE(LOG_ERROR, MODULE_NAME)
			<< "Could not find uniform location name: " << name << LOG_END;
		return location;
	}
	uniformsLocationCache[name] = location;
	
	
	return location;
}

