#ifndef CELLULAR_DISPLAYOBJECT_H
#define CELLULAR_DISPLAYOBJECT_H

#include "../Window.h"
#include "../Camera.h"
#include "Transforms.h"

class DisplayObject {
private:
    Transforms transforms;
public:
	virtual void render(const Window& window, const Camera& camera) = 0;

	void setPosition(const vec3& pos) {
	    transforms.setPosition(pos);
	}
    void setPosition(const vec2& pos) {
        transforms.setPosition(pos);
    }
	void setRotation(float angle, const vec3& axis) {
	    transforms.setRotation(angle, axis);
	}
	void setRotationAngle(float angle) {
	    transforms.setRotationAngle(angle);
	}
	void translate(const vec3& offset) {
	    transforms.translate(offset);
	}
    void translate(const vec2& offset) {
        transforms.translate(offset);
    }

	void setScale(float scaleX, float scaleY, float scaleZ) {
	    transforms.setScale(scaleX, scaleY, scaleZ);
	}
	void setScale(float scale) {
	    transforms.setScale(scale);
	}

	const vec3& getPosition() const {
        return transforms.getPosition();
	};
	const mat4& getTransformMat() const {
        return transforms.getTransformMat();
	};

	void updateTransforms() {
	    transforms.update();
	}

};

#endif //CELLULAR_DISPLAYOBJECT_H
