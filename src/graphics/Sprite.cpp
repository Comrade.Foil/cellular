#include "Sprite.h"

#include "../core/TextureManager.h"

const vector<vec4> Sprite::geometryVertices = vector<vec4> {
	vec4(-0.5, -0.5, 0.0, 1.0), vec4(0.5, -0.5, 0.0, 1.0), vec4(0.5, 0.5, 0.0, 1.0),
	vec4(-0.5, -0.5, 0.0, 1.0), vec4(0.5, 0.5, 0.0, 1.0), vec4(-0.5, 0.5, 0.0, 1.0)
};
const vector<vec2> Sprite::textureCoordinates = vector<vec2> {
	vec2(0.0, 0.0), vec2(1.0, 0.0), vec2(1.0, 1.0),
	vec2(0.0, 0.0), vec2(1.0, 1.0), vec2(0.0, 1.0),
};

void Sprite::render(const Window &window, const Camera &camera) {
    updateTransforms();

    program->use();
    program->setUniform1f("uAlpha", alpha);
	program->setUniformMatrix4fv("uTransforms", getTransformMat());
	program->setUniformMatrix4fv("uMVP", camera.getMVP());
	glActiveTexture(GL_TEXTURE0);
	glBindTexture(GL_TEXTURE_2D, texture->id);
	glBindVertexArray(vao);
	glDrawArrays(GL_TRIANGLES, 0, geometryVertices.size());
	glBindVertexArray(0);
}

Sprite::Sprite(Texture* tex, Shader* shader) {
	texture = tex;
	program = shader;
	initBuffers();
	alpha = 1.0;
}

BufferObject &Sprite::getTextureCoordsBuffer() {
	static BufferObject buff(textureCoordinates, GL_STATIC_DRAW);
	return buff;
}

BufferObject &Sprite::getGeometryBuffer() {
	static BufferObject buff(geometryVertices, GL_STATIC_DRAW);
	return buff;
}

void Sprite::initBuffers() {
	geometryBuffer = Sprite::getGeometryBuffer();
	textureCoordsBuffer = Sprite::getTextureCoordsBuffer();

	glCreateVertexArrays(1, &vao);
	glEnableVertexArrayAttrib(vao, 0);
	glVertexArrayVertexBuffer(vao, 0, geometryBuffer.getId(), 0, sizeof(vec4));
	glVertexArrayAttribFormat(vao, 0, 4, GL_FLOAT, GL_FALSE, 0);
	glEnableVertexArrayAttrib(vao, 1);
	glVertexArrayVertexBuffer(vao, 1, textureCoordsBuffer.getId(), 0, sizeof(vec2));
	glVertexArrayAttribFormat(vao, 1, 2, GL_FLOAT, GL_FALSE, 0);
	glBindVertexArray(0);


	// old-fashioned way without DSA
//	glGenVertexArrays(1, &vao);
//	glBindVertexArray(vao);
//	geometryBuffer.bind();
//	glEnableVertexAttribArray(0);
//	glVertexAttribPointer(0, 4, GL_FLOAT, GL_FALSE, sizeof(vec4), (void*)0);
//	textureCoordsBuffer.bind();
//	glEnableVertexAttribArray(1);
//	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, sizeof(vec2), (void*)0);
//	textureCoordsBuffer.unbind();
}
