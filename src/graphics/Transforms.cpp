#include "Transforms.h"

Transforms::Transforms() {
    position = vec3(0.0, 0.0, 0.0);

    scaleMat = mat4(1.0);

    rotationAxis = vec3(1.0, 0.0, 0.0);
    rotationAngle = 0.0;
    rotationMat = mat4(1.0);

    transformMat = mat4(1.0);
}

void Transforms::setPosition(const vec2 &pos) {
    position = vec3(pos.x, pos.y, 0.0);
}

void Transforms::setPosition(const vec3& pos) {
    position = vec3(pos.x, pos.y, pos.z);
}

void Transforms::setPosition(float x, float y) {
    position = vec3(x, y, 0.0);
}

void Transforms::setPosition(float x, float y, float z) {
    position = vec3(x, y, z);
}

void Transforms::setScale(float scaleX, float scaleY, float scaleZ) {
    scaleMat = mat4(
            vec4(scaleX, 0.0, 0.0, 0.0),
            vec4(0.0, scaleY, 0.0, 0.0),
            vec4(0.0, 0.0, scaleZ, 0.0),
            vec4(0.0, 0.0, 0.0, 1.0)
    );
}

void Transforms::setScale(float scale) {
    setScale(scale, scale, scale);
}

void Transforms::setScale(const vec3& scale) {
    setScale(scale.x, scale.y, scale.z);
}

void Transforms::setRotation(float angle, float x, float y, float z) {
    rotationAngle = angle;
    rotationAxis = vec3(x, y, z);
    updateRotationMat();
}

void Transforms::setRotation(float angle, const vec3& axis) {
    setRotation(angle, axis.x, axis.y, axis.z);
}

void Transforms::translate(const vec2 &offset) {
    position += vec3(offset.x, offset.y, 0.0);
}

void Transforms::translate(const vec3 &offset) {
    position += vec3(offset.x, offset.y, offset.z);
}

void Transforms::update() {
    updateRotationMat();

    transformMat = rotationMat * scaleMat;
    transformMat[3][0] = position.x;
    transformMat[3][1] = position.y;
    transformMat[3][2] = position.z;
}

const vec3 &Transforms::getPosition() const {
    return position;
}

const mat4 &Transforms::getTransformMat() const {
    return transformMat;
}

void Transforms::setRotationAngle(float angle) {
    rotationAngle = angle;
}

void Transforms::updateRotationMat() {
    rotationMat = mat4_cast(angleAxis(rotationAngle, rotationAxis));
}

