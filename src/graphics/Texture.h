#ifndef CELLULAR_TEXTURE_H
#define CELLULAR_TEXTURE_H

#include <GLEW/glew.h>
#include "../core/Logger.h"

struct Texture {
	Texture() = default;
	unsigned int id;
	
	unsigned int width;
	unsigned int height;
	
	GLenum format; // GL_RED or GL_RGB or GL_RGBA
	GLenum type; // GL_DEPTH_COMPONENT or GL_UNSIGNED_BYTE
	
	~Texture() {
		glDeleteTextures(1, &id);
	}
	
	/*int minFilter;
	int magFilter;
	int wrapModeHorizontal;
	int wrapModeVertical;
	
	unsigned int bufferId;*/
	
};


#endif //CELLULAR_TEXTURE_H
