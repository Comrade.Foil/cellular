#ifndef FIESTA_CAMERA_H
#define FIESTA_CAMERA_H

#include <glm/glm.hpp>
#include <glm/gtx/transform.hpp>
#include <glm/detail/type_mat.hpp>
#include <glm/detail/type_mat4x4.hpp>
#include <glm/vec3.hpp>
#include <GLEW/glew.h>
#include <GLFW/glfw3.h>
#include "Window.h"

using namespace glm;

class Camera {
private:
    mat4 viewMatrix, modelMatrix, projMatrix, MVP;
    vec3 position, direction;
    GLfloat camSpd, mouseSpd;
    GLdouble cursorStartX, cursorStartY, horAngle, vertAngle;
    bool RMB_DOWN;

//    Window* m_window;
    
    const AppConfig& config;
    
    vec3 calculateDirection() const;
public:
    explicit Camera(const AppConfig& config);
    ~Camera();
    
    mat4 rotation;
    mat4 translation;

    mat4 getMVP() const;
    void update(GLdouble dTime, const Window& window);
    mat4 getViewMatrix() const;
    mat4 getProjectionMatrix() const;
    vec3 getPosition() const;
    vec3 getDirection() const;
};


#endif //FIESTA_CAMERA_H
