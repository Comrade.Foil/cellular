#include "Blob.h"

void Blob::update(const Window& window, const World& world) {
	physicsComponent.update(*this, world, window);
}

void Blob::draw(const Window& window, const Camera& camera) {
    morphShape(window.dTime, window.globalTime);

    il->setPosition(position);
    il->render(window, camera);
}

Blob::Blob(float radius, float thickness, int resolution, Shader* morphingCompute) {
	this->compute = morphingCompute;
	this->radius = radius;
	this->resolution = resolution;

	initializeCircleGeometry(thickness);

	normalsBuffer.allocate(normals, GL_STATIC_DRAW);
	il = new InstancedLines(points, thickness, InstancedLines::STRIP);
}

Blob::~Blob() {
	delete il;
//	cout << "blob " << this->getId() << " deleted" << endl;
}

void Blob::initializeCircleGeometry(float thickness) {
	vec2 initial = vec2(1.0, 0.0);
	for (int i = 0; i < resolution; i++) {

		float x = cos (i * (2 * glm::pi<float>() / resolution));
		float y = sin (i * (2 * glm::pi<float>() / resolution));
		vec2 point = vec2(x, y);
		normals.push_back(point);

		point *= (radius - thickness/2);
		points.push_back(point);
	}

	vec2 final = vec2(1.0, 0.0);
	normals.push_back(final);
	final *= (radius - thickness/2);
	points.push_back( final);
}

void Blob::morphShape(float dTime, float globalTime) {
    BufferObject pointsBuffer = il->getPointsBuffer();

    compute->use();
    compute->setUniform1i("uBufferSize", points.size());
    compute->setUniform1f("uTime", globalTime);
    pointsBuffer.bind(GL_SHADER_STORAGE_BUFFER);
    pointsBuffer.bindBase(0);
    normalsBuffer.bind(GL_SHADER_STORAGE_BUFFER);
    normalsBuffer.bindBase(1);
    
    glDispatchCompute(points.size(), 1, 1);
    pointsBuffer.unbind();
}
