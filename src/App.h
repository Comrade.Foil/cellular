#ifndef CELLULAR_APP_H
#define CELLULAR_APP_H

#include <entt/entity/registry.hpp>
#include "Window.h"
#include "Camera.h"
#include "CellularField.h"
#include "graphics/Line.h"
#include "graphics/InstancedLines.h"
#include "core/World.h"

class App {
private:
	static const string MODULE_NAME;
	const AppConfig& appConfig;
	void init2DGrid();

public:
	Window m_window;
	Camera m_camera;
	World m_world;
	entt::registry m_reg;

	App(const AppConfig& config);
	~App();

	void setup();
	void start();
	void exit();
};


#endif //CELLULAR_APP_H
