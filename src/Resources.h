#ifndef CELLULAR_RESOURCES_H
#define CELLULAR_RESOURCES_H

#include "core/ShaderManager.h"
#include "core/TextureManager.h"
#include "core/ModelsManager.h"

namespace Resources {
	void loadResources() {
		Shader *simpleLineShader = new Shader(
			"simpleLine",
			"resources//shaders//colorVertex.glsl",
			"resources//shaders//colorFragment.glsl"
		);
		Shader *simpleColorShader = new Shader(
			"debugLine",
			"resources//shaders//debugLineVertex.glsl",
			"resources//shaders//debugLineFragment.glsl"
		);
		Shader *blobShader = new Shader("blob", "resources//shaders//stubVertex.glsl",
										"resources//shaders//blobsFragment.glsl");
		Shader *simpleShader = new Shader("simple", "resources//shaders//simpleVertex.glsl",
										  "resources//shaders//simpleFragment.glsl");
		Shader *skyboxShader = new Shader("skybox", "resources//shaders//skyboxVertex.glsl",
										  "resources//shaders//skyboxFragment.glsl");
		Shader *morphingCompute = new Shader("morphingCompute", "resources//shaders//morphingCompute.glsl");
		Shader *spriteShader = new Shader("sprite", "resources//shaders//spriteVertex.glsl",
										  "resources//shaders//spriteFragment.glsl");
		
		Shader *instancedLines = new Shader(
			"instancedLines",
			"resources//shaders//InstancedLinesVertex.glsl",
			"resources//shaders//InstancedLinesFragment.glsl"
		);
		Shader *gameOfLife = new Shader("gameOfLife", "resources//automatas//gameOfLife.glsl");
		Shader *growingColony = new Shader("growingColony", "resources//automatas//3dAutomata.glsl");
		
		ShaderManager::addShader(simpleLineShader);
		ShaderManager::addShader(blobShader);
		ShaderManager::addShader(simpleShader);
		ShaderManager::addShader(skyboxShader);
		ShaderManager::addShader(morphingCompute);
		ShaderManager::addShader(instancedLines);
		ShaderManager::addShader(gameOfLife);
		ShaderManager::addShader(growingColony);
		ShaderManager::addShader(spriteShader);
		ShaderManager::addShader(simpleColorShader);
		
		
		vector<std::string> faces
			{
				"resources//textures//skybox//right.jpg",
				"resources//textures//skybox//left.jpg",
				"resources//textures//skybox//top.jpg",
				"resources//textures//skybox//bottom.jpg",
				"resources//textures//skybox//front.jpg",
				"resources//textures//skybox//back.jpg"
			};
		TextureManager::loadCubeMap(faces, "skybox");
		TextureManager::loadTexture("resources//textures//noise.jpg", "noise");
		TextureManager::loadTexture("resources//textures//hat.bmp", "hat");
		TextureManager::loadTexture("resources//textures//skybox//back.jpg", "back");
		
		ModelsManager::loadModel("resources//models//cube.obj", "cube");
	}
}

#endif //CELLULAR_RESOURCES_H
