
#ifndef CELLULAR_MESHDATA_H
#define CELLULAR_MESHDATA_H

#include <vector>
#include <GLEW/glew.h>
#include <string>

#include "glm/vec3.hpp"
#include "glm/vec2.hpp"

using namespace std;
using namespace glm;

class MeshData {
public:
	vector<GLfloat> vertices;
	vector<GLfloat> normals;
	vector<GLuint> indices;
	vector<GLfloat> texCoords;
	vector<GLfloat> colors;
//	vector<string> textures;
	
	MeshData() {
	
	}
	
	~MeshData() {
		vector<GLfloat>().swap(vertices);
		vector<GLfloat>().swap(colors);
		vector<GLfloat>().swap(texCoords);
		vector<GLuint>().swap(indices);
//		vector<string>().swap(textures);
	}
	
	void pushVertex(vec3 v) {
		vertices.push_back(v.z);
		vertices.push_back(v.y);
		vertices.push_back(v.x);
	}
	
	void pushNormal(vec3 n) {
		normals.push_back(n.z);
		normals.push_back(n.y);
		normals.push_back(n.x);
	}
	
	void pushVertexColor(vec3 c) {
		colors.push_back(c.z);
		colors.push_back(c.y);
		colors.push_back(c.x);
	}
	
	void pushTexcoord(vec2 t) {
		texCoords.push_back(t.y);
		texCoords.push_back(t.x);
	}
	
	void pushIndex(GLuint i) {
		indices.push_back(i);
	}
	
//	void pushTexture(string s) {
//		textures.push_back(s);
//	}
	
	GLulong getVerticesNum() {
		return (vertices.size() / 3);
	}
	
	GLulong getIndicesNum() {
		return indices.size();
	}
	
};


#endif //CELLULAR_MESHDATA_H
