#ifndef CELLULAR_BLOB_H
#define CELLULAR_BLOB_H

#include "core/Entity.h"
#include "graphics/InstancedLines.h"
#include "Window.h"
#include "systems/SMover.h"

class Blob : public Entity {
private:
	InstancedLines* il;
	vector<vec2> points;
	vector<vec2> normals;
	BufferObject normalsBuffer;
	MoverSystem physicsComponent;
	unsigned int resolution;
	float radius;
	Shader* compute;

	void initializeCircleGeometry(float thickness);
    void morphShape(float dTime, float globalTime);
public:

	Blob(float radius, float thickness, int resolution, Shader* morphingCompute);
	void update(const Window& window, const World& world) override;
	void draw(const Window& window, const Camera& camera) override;

	~Blob();
};


#endif //CELLULAR_BLOB_H
