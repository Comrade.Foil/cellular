#include "SPlayerControls.h"

#include "algorithm"
#include "../Blob.h"
#include "../core/TextureManager.h"
#include "../core/ShaderManager.h"

std::unordered_map<unsigned int, weak_ptr<Entity>> SPlayerControls::entities = std::unordered_map<unsigned int, weak_ptr<Entity>>();
std::vector<int> SPlayerControls::pressedButtons = std::vector<int>();

void SPlayerControls::update(const Window &window, World &world) {
	for (auto &mapEntry : SPlayerControls::entities) {
		if (!mapEntry.second.expired()) {
			auto entityPointer = mapEntry.second.lock();
			entityPointer->setAcceleration(vec3(0.0));
		}
	}
	
	if (glfwGetKey(window.windowContext, GLFW_KEY_RIGHT) == GLFW_PRESS) {
		for (auto &mapEntry : SPlayerControls::entities) {
			if (!mapEntry.second.expired()) {
				auto entityPointer = mapEntry.second.lock();
				entityPointer->addAcceleration(vec3(1, 0.0, 0.0) * (float) window.dTime);
			}
		}
	}
	
	if (glfwGetKey(window.windowContext, GLFW_KEY_UP) == GLFW_PRESS) {
		for (auto &mapEntry : SPlayerControls::entities) {
			if (!mapEntry.second.expired()) {
				auto entityPointer = mapEntry.second.lock();
				entityPointer->addAcceleration(vec3(0.0, 1.0, 0.0) * (float) window.dTime);
			}
		}
	}
	
	if (glfwGetKey(window.windowContext, GLFW_KEY_DOWN) == GLFW_PRESS) {
		for (auto &mapEntry : SPlayerControls::entities) {
			if (!mapEntry.second.expired()) {
				auto entityPointer = mapEntry.second.lock();
				entityPointer->addAcceleration(vec3(0.0, -1.0, 0.0) * (float) window.dTime);
			}
		}
	}
	
	if (glfwGetKey(window.windowContext, GLFW_KEY_LEFT) == GLFW_PRESS) {
		for (auto &mapEntry : SPlayerControls::entities) {
			if (!mapEntry.second.expired()) {
				auto entityPointer = mapEntry.second.lock();
				entityPointer->addAcceleration(vec3(-1, 0.0, 0.0) * (float) window.dTime);
			}
		}
	}
	
//	if (glfwGetKey(m_window.w, GLFW_KEY_LEFT_CONTROL) == GLFW_PRESS) {
//		for (auto &mapEntry : SPlayerControls::entities) {
//			if (!mapEntry.second.expired()) {
//				auto entityPointer = mapEntry.second.lock();
//				shared_ptr<Entity> projectile = make_shared<Blob>(
//					0.2,
//					0.1,
//					8.0,
//					ShaderManager::getShader("morphingCompute"),
//					TextureManager::getTexture("noise")
//				);
//				projectile->setVelocity(entityPointer->getVelocity());
//				projectile->setPosition(entityPointer->getPosition());
//				projectile->setAcceleration(projectile->getVelocity() * 5.0f);
//				world.addEntity(projectile);
//			}
//		}
//	}
}

void SPlayerControls::registerEntity(shared_ptr<Entity> &e) {
	SPlayerControls::entities.insert(make_pair(e->getId(), e));
}

void SPlayerControls::removeEntity(unsigned int id) {

}
