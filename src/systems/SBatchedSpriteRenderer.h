#ifndef CELLULAR_SBATCHEDSPRITERENDERER_H
#define CELLULAR_SBATCHEDSPRITERENDERER_H

#include "../components/CTexture.h"
#include "../core/NonCopyable.h"

#include <vector>
#include "glm/vec2.hpp"
#include "glm/vec4.hpp"

using namespace Component;
using namespace glm;
using namespace std;

class SBatchedSpriteRenderer : public NonCopyable {
private:
	const static vector<vec2> textureCoordinates;
	const static vector<vec4> geometryVertices;
};


#endif //CELLULAR_SBATCHEDSPRITERENDERER_H
