#ifndef CELLULAR_SBATCHEDDEBUGLINERENDERER_H
#define CELLULAR_SBATCHEDDEBUGLINERENDERER_H


#include "../core/NonCopyable.h"
#include "entt/entity/registry.hpp"
#include "../components/CDebugLine.h"
#include "../Window.h"
#include "../Camera.h"
#include "../graphics/BufferObject.h"


using namespace Component;

class SBatchedDebugLineRenderer : public NonCopyable {
private:
	SBatchedDebugLineRenderer() = default;
	
	static BufferObject verticesBO;
	static BufferObject colorsBO;
	static BufferObject transformsBO;
	static BufferObject mappingBO;
	static inline unsigned int vao = 0;
	static inline unsigned int entitiesCount = 0;
	
	static bool shouldUpdateBuffer(entt::registry &reg);
	static void reallocateBuffers(entt::registry& reg);
	
	static void initVertexArrayObject(entt::registry &reg);
	static void render(unsigned int linesCount, const Window &window, const Camera &camera);

public:
	static void update(entt::registry& reg, const Window& window, const Camera& camera);
};


#endif //CELLULAR_SBATCHEDDEBUGLINERENDERER_H
