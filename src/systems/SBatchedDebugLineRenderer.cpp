#include "SBatchedDebugLineRenderer.h"

#include "vector"
#include "../core/ShaderManager.h"
#include "../components/CTransform.h"

BufferObject SBatchedDebugLineRenderer::verticesBO = BufferObject();
BufferObject SBatchedDebugLineRenderer::colorsBO = BufferObject();
BufferObject SBatchedDebugLineRenderer::transformsBO = BufferObject();
BufferObject SBatchedDebugLineRenderer::mappingBO = BufferObject();

using namespace Component;

void SBatchedDebugLineRenderer::update(entt::registry &reg, const Window &window, const Camera &camera) {
	auto view = reg.view<Component::CDebugLine>();
	
	if (!verticesBO.isCreated() && !colorsBO.isCreated()) {
		initVertexArrayObject(reg);
	}
	
	if (shouldUpdateBuffer(reg)) {
		reallocateBuffers(reg);
	}
	int linesNumber = view.size();
	render(linesNumber, window, camera);
	
}

bool SBatchedDebugLineRenderer::shouldUpdateBuffer(entt::registry &reg) {
	auto view = reg.view<Component::CDebugLine>();
	long int totalVerticesSize = view.size() * 2 * sizeof(vec3);
	
	return verticesBO.isCreated() && colorsBO.isCreated() && totalVerticesSize > verticesBO.getDataSize();
}

void SBatchedDebugLineRenderer::reallocateBuffers(entt::registry &reg) {
	auto group = reg.group<CDebugLine, CTransform>();
	auto vertices = vector<vec3>();
	auto colors = vector<vec4>();
	auto transforms = vector<glm::mat4>();
	auto mappingArray = vector<int>();
	
	for (const entt::entity e : group) {
		CDebugLine &data = reg.get<CDebugLine>(e);
		CTransform &transform = reg.get<CTransform>(e);
		
		for (int i = 0; i < data.vertices.size(); i++) {
			vertices.push_back(data.vertices[i]);
			colors.push_back(data.colors[i]);
		}
		
		if (mappingArray.empty()) {
			mappingArray.push_back(vertices.size());
		} else {
			int prevCount = mappingArray[mappingArray.size() - 1];
			mappingArray.push_back(prevCount + vertices.size());
		}
		
		transforms.push_back(transform.getTransformMatrix());
	}
	
	entitiesCount = group.size();
	
	mappingBO.reallocate(mappingArray, GL_DYNAMIC_DRAW);
	verticesBO.reallocate(vertices, GL_DYNAMIC_DRAW);
	colorsBO.reallocate(colors, GL_DYNAMIC_DRAW);
	transformsBO.reallocate(transforms, GL_DYNAMIC_DRAW);
	
	transformsBO.unbind();
}


void SBatchedDebugLineRenderer::initVertexArrayObject(entt::registry &reg) {
	auto group = reg.group<CDebugLine, CTransform>();
	auto vertices = vector<vec3>();
	auto colors = vector<vec4>();
	auto transforms = vector<glm::mat4>();
	auto mappingArray = vector<int>();
	auto transformsMapping = vector<unsigned int>();
	
	for (const entt::entity e : group) {
		CDebugLine &data = reg.get<CDebugLine>(e);
		CTransform &transform = reg.get<CTransform>(e);
		
		for (int i = 0; i < data.vertices.size(); i++) {
			vertices.push_back(data.vertices[i]);
			colors.push_back(data.colors[i]);
		}
		
		if (mappingArray.empty()) {
			mappingArray.push_back(vertices.size());
		} else {
			int prevCount = mappingArray[mappingArray.size() - 1];
			mappingArray.push_back(prevCount + vertices.size());
		}
		
		transforms.push_back(transform.getTransformMatrix());
	}
	
	entitiesCount = group.size();
	
	mappingBO.allocate(mappingArray, GL_DYNAMIC_DRAW);
	verticesBO.allocate(vertices, GL_DYNAMIC_DRAW);
	colorsBO.allocate(colors, GL_DYNAMIC_DRAW);
	transformsBO.allocate(transforms, GL_DYNAMIC_DRAW);
	
	transformsBO.unbind();
	
	glCreateVertexArrays(1, &vao);
	glVertexArrayVertexBuffer(vao, 0, verticesBO.getId(), 0, sizeof(vec3));
	glVertexArrayVertexBuffer(vao, 1, colorsBO.getId(), 0, sizeof(vec4));

	glEnableVertexArrayAttrib(vao, 0);
	glEnableVertexArrayAttrib(vao, 1);

	glVertexArrayAttribFormat(vao, 0, 3, GL_FLOAT, GL_FALSE, 0);
	glVertexArrayAttribFormat(vao, 1, 4, GL_FLOAT, GL_FALSE, 0);

	glVertexArrayAttribBinding(vao, 0, 0);
	glVertexArrayAttribBinding(vao, 1, 1);
	glBindVertexArray(0);
}

void SBatchedDebugLineRenderer::render(unsigned int linesCount, const Window &window, const Camera &camera) {
	Shader *shader = GET_SHADER("debugLine");
	
	if (shader == nullptr) {
		return;
	}
	
	shader->use();
	transformsBO.bindBase(GL_SHADER_STORAGE_BUFFER, 1);
	mappingBO.bindBase(GL_SHADER_STORAGE_BUFFER, 2);
	shader->setUniformMatrix4fv("uMVP", camera.getMVP());
	shader->setUniform1i("uEntitiesCount", entitiesCount);
	glBindVertexArray(vao);
	glDrawArrays(GL_LINES, 0, linesCount * 2); // 2 vertices per line
	
	transformsBO.unbindBase(1);
}
