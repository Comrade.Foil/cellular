#ifndef CELLULAR_SPLAYERCONTROLS_H
#define CELLULAR_SPLAYERCONTROLS_H

#include "vector"
#include "../core/NonCopyable.h"
#include "../Window.h"
#include "../core/World.h"

class SPlayerControls : public NonCopyable {
private:
	static vector<int> pressedButtons;
    SPlayerControls() = default;
    static unordered_map<unsigned int, weak_ptr<Entity>> entities;

public:
    static void update(const Window& window, World& world);
    
    static void registerEntity(shared_ptr<Entity>& e);
    static void removeEntity(unsigned int id);
};


#endif //CELLULAR_SPLAYERCONTROLS_H
