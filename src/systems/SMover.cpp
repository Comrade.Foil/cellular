#include "SMover.h"

#include "glm/gtc/random.hpp"

void MoverSystem::update(Entity &entity, const World &world, const Window &window) {
	// periodic boundary of the scene
	if (entity.getPosition().x > world.getSize().x || entity.getPosition().x < 0) {
		entity.setPosition(vec3(
			abs(entity.getPosition().x - world.getSize().x),
			entity.getPosition().y,
			0.0f)
		);
	} else if (entity.getPosition().y > world.getSize().y || entity.getPosition().y < 0) {
		entity.setPosition(vec3(
			entity.getPosition().x,
			abs(entity.getPosition().y - world.getSize().y),
			0.0f)
		);
	}
	
	

//	vec3 a = normalize(vec3(linearRand(-1.0, 1.0), linearRand(-1.0, 1.0), 0.0));

//	entity.setAcceleration(a);
	entity.setVelocity(entity.getVelocity() + entity.getAcceleration());
	if (length(entity.getVelocity()) > 10.0) {
		entity.setVelocity(normalize(entity.getVelocity()) * 10.0f);
	}
	entity.setPosition(entity.getPosition() + (entity.getVelocity() * (float) window.dTime));
}
