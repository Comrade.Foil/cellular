#ifndef CELLULAR_SMOVER_H
#define CELLULAR_SMOVER_H

#include "glm/vec3.hpp"
#include "../core/Entity.h"
#include "../core/World.h"

using namespace glm;

class MoverSystem {
public:
	void update(Entity& entity, const World& world, const Window& window);
};


#endif //CELLULAR_SMOVER_H
