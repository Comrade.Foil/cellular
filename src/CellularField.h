#ifndef CELLULAR_CELLULARFIELD_H
#define CELLULAR_CELLULARFIELD_H

#include <vector>
#include <cstdlib>
#include <glm/detail/type_vec.hpp>
#include "glm/vec2.hpp"
#include <string>
#include <iostream>
#include "time.h"
#include "graphics/Shader.h"
#include "graphics/BufferObject.h"
#include "core/Entity.h"


using namespace std;
using namespace glm;

class CellularField {
private:
	vector<int> backBuffer;
	const double UPDATE_DELAY = 0.6;
	double timePassed = 0;

	void init(unsigned int, unsigned int);
	void init(unsigned int, unsigned int, unsigned int);

	int getBackCell(unsigned int x, unsigned int y) const;
	int getBackCell(ivec2 pos) const;
	int getBackCell(unsigned int x, unsigned int y, unsigned int z) const;
	int getBackCell(ivec3 pos) const;

//    int getLivingNeighbours(ivec2 position);

public:
	Shader* compute;
	BufferObject backBO, frontBO;
	vector<int> frontBuffer;
	vector<vec4> alivePositions;
	int width, height, depth;

	CellularField(unsigned int width, unsigned int height, const string& computeShader);
	CellularField(unsigned int width, unsigned int height, unsigned int depth, const string& computeShader);
	CellularField();

	int getCell(unsigned int x, unsigned int y) const;
	int getCell(ivec2 pos) const;
	void setCell(unsigned int x, unsigned int y, int val);
	void setCell(ivec2 pos, int val);

	int getCell(unsigned int x, unsigned int y, unsigned int z) const;
	int getCell(ivec3 pos) const;
	void setCell(unsigned int x, unsigned int y, unsigned int z, int val);
	void setCell(ivec3 pos, int val);

	void obviousPattern();
	void randomize();
	void updateLivingCells();
	void update(double dt);
	void draw(double dt);
};


#endif
