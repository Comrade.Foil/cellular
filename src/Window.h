#ifndef WINDOW_H
#define WINDOW_H

#include <string>
#include <iostream>

#include <GLEW/glew.h>
#include <GLFW/glfw3.h>
#include <glm/vec4.hpp>
#include "core/AppConfig.h"
#include "core/Logger.h"

using namespace std;

class Window {
public:
	static string MODULE_NAME;

	double startUpTime;
	double globalTime;
	double dTime;
    double lastUpdateTime;
	bool glDebugEnabled;
	
	GLFWwindow* windowContext;

    Window(const AppConfig& config);
    ~Window();

    void resizeFrameBuffer();
    void setClearColor(float, float, float, float);
    bool shouldClose();
    void update();

private:
    glm::vec4 color = glm::vec4(0.0);
    void initImGui();
    void initGLFW(const AppConfig& config);
    void initGLEW();
	
	void updateImGui();
};


#endif //WINDOW_H
