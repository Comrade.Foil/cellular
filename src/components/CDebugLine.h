#ifndef CELLULAR_CDEBUGLINE_H
#define CELLULAR_CDEBUGLINE_H

#include "glm/vec3.hpp"
#include "glm/vec4.hpp"

using namespace std;
using namespace glm;

namespace Component {
	struct CDebugLine {
		vector<vec3> vertices;
		vector<vec4> colors;
		
		CDebugLine() = default;
		CDebugLine(const CDebugLine &other) {
			vertices = other.vertices;
			colors = other.colors;
		};
		CDebugLine &operator=(CDebugLine &&other) {
			vertices = other.vertices;
			colors = other.colors;
		}
		CDebugLine(const glm::vec3 &p1, const glm::vec3 &p2, const glm::vec4 &c1, const glm::vec4 &c2) {
			addPoint(p1, c1);
			addPoint(p2, c2);
		};

		void addPoint(const vec3 &vertex, const vec4 &color) {
			vertices.push_back(vertex);
			colors.push_back(color);
		};
		
		void addPoint(const vec3 &vertex) {
			vertices.push_back(vertex);
			if (colors.empty()) {
				colors.push_back(vec4(1.0, 0.0, 0.0, 1.0));
			} else {
				colors.push_back(vec4(colors[colors.size() - 1]));
			}
		};
		
	};
}

#endif //CELLULAR_CDEBUGLINE_H
