#ifndef CELLULAR_CTRANSFORM_H
#define CELLULAR_CTRANSFORM_H

#include "glm/vec3.hpp"
#include "glm/mat4x4.hpp"
#include "glm/gtx/quaternion.hpp"
#include <glm/gtc/matrix_transform.hpp>
namespace Component {
	struct CTransform {
		glm::vec3 translation = {0.0f, 0.0f, 0.0f};
		glm::vec3 rotation = {0.0f, 0.0f, 0.0f};
		glm::vec3 scale = {1.0f, 1.0f, 1.0f};
		
		CTransform() = default;
		CTransform(const CTransform &) = default;
		CTransform(const glm::vec3 &t) : translation(t) {};
		
		glm::mat4 getTransformMatrix() {
			auto res = glm::translate(glm::mat4(1.0f), translation)
					   * glm::toMat4(glm::quat(rotation))
					   * glm::scale(glm::mat4(1.0f), scale);
			return res;
		}
		
	};
}



#endif //CELLULAR_CTRANSFORM_H
