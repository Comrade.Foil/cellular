#ifndef CELLULAR_CTEXTURE_H
#define CELLULAR_CTEXTURE_H

#include "string"

using namespace std;

namespace Component {
	struct CTexture {
		string textureName;
		
		CTexture() = default;
		CTexture(const CTexture &) = default;
		CTexture(const string& name) : textureName(name) {};
	};
}

#endif //CELLULAR_CTEXTURE_H
