#ifndef CELLULAR_CTAG_H
#define CELLULAR_CTAG_H

#include "string"

struct CTag
{
	std::string tag;
	
	CTag() = default;
	CTag(const CTag&) = default;
	CTag(const std::string& val)
		: tag(val) {}
};

#endif //CELLULAR_CTAG_H
