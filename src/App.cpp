#include "glm/gtc/random.hpp"

#include "App.h"
#include "Blob.h"
#include "graphics/Sprite.h"
#include "systems/SPlayerControls.h"

#include "core/ShaderManager.h"
#include "core/TextureManager.h"
#include "core/ModelsManager.h"

#include "EntityFactory.h"

#include "Resources.h"

using namespace EntityFactory;

const string App::MODULE_NAME = "App";

App::App(const AppConfig &config) : m_window(config), m_camera(config), m_world(config), appConfig(config) {
	m_window.setClearColor(
		appConfig.backgroundColor.x,
		appConfig.backgroundColor.y,
		appConfig.backgroundColor.z,
		appConfig.backgroundColor.w
	);
	
	Resources::loadResources();
}

void App::setup() {
	init2DGrid();
	
	shared_ptr<Entity> player = make_shared<Blob>(6.5, 0.2, 256, GET_SHADER("morphingCompute"));
	player->setPosition(vec3(16.0, 16.0, 0.0));
	m_world.addEntity(player);
	SPlayerControls::registerEntity(player);
	
	vector<Blob *> blobs;
	
	for (int i = 0; i < 5; i++) {
		shared_ptr<Entity> b = make_shared<Blob>(
			linearRand(0.2, 0.4),
			linearRand(0.02, 0.1),
			linearRand(4, 10),
			GET_SHADER("morphingCompute")
		);
		b->setPosition(vec3(linearRand(1.0, 30.0), linearRand(1.0, 30.0), 0.0));
		m_world.addEntity(b);
	}
	// initialize game systems and objects
}

void App::start() {
	vector<float> cubeVertices = GET_MODEL("cube")->vertices;
	
	
	CellularField cellularField = CellularField(64, 64, 1, "gameOfLife");
	cellularField.randomize();
	
	cellularField.updateLivingCells();
	
	GLuint cellsVAO;
	glGenVertexArrays(1, &cellsVAO);
	glBindVertexArray(cellsVAO);
	
	BufferObject vertexBuffer;
	vertexBuffer.allocate(cubeVertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), (void *) 0);
	vertexBuffer.unbind();
	
	BufferObject cellsPositionsBO;
	cellsPositionsBO.allocate(cellularField.alivePositions, GL_STREAM_DRAW);
	glVertexAttribPointer(1, 4, GL_FLOAT, GL_FALSE, sizeof(vec4), (void *) 0);
	glVertexAttribDivisor(1, 1);
	cellsPositionsBO.unbind();
	glBindVertexArray(0);
	
	GLuint skyboxVAO;
	vertexBuffer.bind();
	glGenVertexArrays(1, &skyboxVAO);
	glBindVertexArray(skyboxVAO);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(vec3), (void *) 0);
	vertexBuffer.unbind();
	
	
	vector<vec2> points = {
		vec2(2.0, 2.0),
		vec2(2.0, -1.0),
		vec2(4.0, 2.0),
		vec2(6.0, -1.0),
		vec2(6.0, 2.0),
		vec2(8.0, -1.0),
		vec2(8.0, 2.0),
		vec2(10.0, -1.0)
	};
	
	InstancedLines il(points, .2, InstancedLines::LINES);
	
	Sprite sprite(GET_TEXTURE("back"), GET_SHADER("sprite"));
	sprite.alpha = 0.8;
	sprite.setPosition(vec3(0.0, 0.0, 0.0));
	sprite.setScale(2.0);
//	sprite.setRotation(radians(15.0f), vec3(1.0, 0.0, 0.0));
	
	while (!m_window.shouldClose()) {
		m_window.update();
		m_camera.update(m_window.dTime, m_window);
		
		// updates
		// draws
		
		

//		{ // skybox
//			Shader* skyboxShader = ShaderManager::getShader("skybox");
//			glDepthMask(GL_FALSE);
//			glUseProgram(skyboxShader->getProgramId());
//			skyboxShader->setUniformMatrix4fv("projection", camera.getProjectionMatrix());
//			skyboxShader->setUniformMatrix4fv("view", glm::mat4(glm::mat3(camera.getViewMatrix())));
//			glBindVertexArray(skyboxVAO);
//			glEnableVertexAttribArray(0);
//			glBindTexture(GL_TEXTURE_CUBE_MAP, TextureManager::getCubeMap("skybox"));
//			glDrawArrays(GL_TRIANGLES, 0, 36);
//			glDepthMask(GL_TRUE);
//		}

//		{ // origin
//			for (auto item : gridLines) {
//				item->render(m_window, m_camera);
//			}
//		}
		
//		{ // update cells
//			Shader *simpleShader = ShaderManager::getShader("simple");
//			cellularField.update(m_window.dTime);
//			cellsPositionsBO.bind(GL_ARRAY_BUFFER);
//			cellsPositionsBO.setData(
//				cellularField.alivePositions.size() * sizeof(vec4),
//				cellularField.alivePositions.data(),
//				GL_STREAM_DRAW
//			);
//
//			glUseProgram(simpleShader->getProgramId());
//			glBindVertexArray(cellsVAO);
//			glEnableVertexAttribArray(0);
//			glEnableVertexAttribArray(1);
//			glEnableVertexAttribArray(2);
//			simpleShader->setUniformMatrix4fv("uMVP", m_camera.getMVP());
//			glDrawArraysInstanced(GL_TRIANGLES, 0, 36, cellularField.alivePositions.size());
//			glBindVertexArray(0);
//		}
		
		{
			m_world.update(m_reg, m_window, m_camera);
		}
		
		{
			il.render(m_window, m_camera);
		}
		
		{
			sprite.render(m_window, m_camera);
		}

//		{ // ray marching scene
//			glUseProgram(blobShader.getProgramId());
//
//			cellsPositionsBO.bindBase(GL_SHADER_STORAGE_BUFFER, 0);
//
//			blobShader.setUniform3f("uCameraPos", camera.getPosition());
//			blobShader.setUniform3f("uCameraDir", camera.getDirection());
//			blobShader.setUniform2f("uResolution", (float) m_window.width, (float) m_window.height);
//			blobShader.setUniform1f("uTime", frameTime);
//			blobShader.setUniform1i("uBlobsAmount", cellularField.alivePositions.size());
//			blobShader.setUniformMatrix4fv("uView", camera.getViewMatrix());
//
//			glDrawArrays(GL_TRIANGLES, 0, 3);
//
//			cellsPositionsBO.unbindBase(GL_SHADER_STORAGE_BUFFER,0);
//		}
	}
	
	exit();
}

void App::exit() {
	ShaderManager::free();
	TextureManager::free();
	ModelsManager::free();
}

App::~App() {

}

void App::init2DGrid() {
	vec4 grey = vec4(0.5, 0.5, 0.5, 0.2);
	vec4 red = vec4(1.0, 0.0, 0.0, 1.0);
	vec4 green = vec4(0.0, 1.0, 0.0, 1.0);
	vec4 blue = vec4(0.0, 0.0, 1.0, 1.0);
	float zValue = -0.1;
	
	for (int i = 0; i < this->m_world.getSize().x; i++) {
		vec3 first = vec3(i, 0.0, 0.0);
		vec3 second = vec3(i, this->m_world.getSize().y, zValue);
		CDebugLine lineDescriptor(first, second, grey, grey);
		
		makeDebugLine(m_reg, lineDescriptor);
	}
	
	for (int i = 0; i < this->m_world.getSize().y; i++) {
		vec3 first = vec3(0.0, i, 0.0);
		vec3 second = vec3(this->m_world.getSize().x, i, zValue);
		CDebugLine lineDescriptor(first, second, grey, grey);
		
		makeDebugLine(m_reg, lineDescriptor);
	}
	
	CDebugLine lineDescriptor(vec3(-ALOT, 0.0, zValue), vec3(ALOT, 0.0, zValue), red, red);
	makeDebugLine(m_reg, lineDescriptor); // X
	lineDescriptor = CDebugLine(vec3(0.0, -ALOT, zValue), vec3(0.0, ALOT, zValue), green, green);
	makeDebugLine(m_reg, lineDescriptor); // Y
	lineDescriptor = CDebugLine(vec3(0.0, 0.0, zValue), vec3(0.0, 0.0, ALOT), blue, blue);
	makeDebugLine(m_reg, lineDescriptor); // Z
	
	lineDescriptor = CDebugLine(vec3(32.0, -ALOT, zValue), vec3(32.0, ALOT, zValue), green, green);
	makeDebugLine(m_reg, lineDescriptor); // X border
	lineDescriptor = CDebugLine(vec3(-ALOT, 32.0, zValue), vec3(ALOT, 32.0, zValue), red, red);
	makeDebugLine(m_reg, lineDescriptor); // Y border
}
