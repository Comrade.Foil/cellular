#include "core/ShaderManager.h"

#include "CellularField.h"

void CellularField::init(unsigned int width, unsigned int height) {
	this->width = width;
	this->height = height;
	this->depth = 1;

	backBuffer = vector<int>(this->width * this->height * this->depth, 0);
	frontBuffer = vector<int>(this->width * this->height * this->depth, 0);

	updateLivingCells();

	backBO.create(GL_SHADER_STORAGE_BUFFER);
    frontBO.create(GL_SHADER_STORAGE_BUFFER);
}

void CellularField::init(unsigned int width, unsigned int height, unsigned int depth) {
	this->width = width;
	this->height = height;
	this->depth = depth;

	backBuffer = vector<int>(this->width * this->height * this->depth, 0);
	frontBuffer = vector<int>(this->width * this->height * this->depth, 0);

	updateLivingCells();

	backBO.create(GL_SHADER_STORAGE_BUFFER);
	frontBO.create(GL_SHADER_STORAGE_BUFFER);
}

CellularField::CellularField(unsigned int width, unsigned int height, const string& computeShader) {
	compute = ShaderManager::getShader(computeShader);
	
	srand(time(NULL));

	init(width, height);
}

CellularField::CellularField(unsigned int width, unsigned int height, unsigned int depth, const string& computeShader) {
	compute = ShaderManager::getShader(computeShader);
	
	srand(time(NULL));

	init(width, height, depth);
}


CellularField::CellularField() {
	srand(time(NULL));

	int width = rand() % 50 + 10;
	int height = rand() % 50 + 10;

	init(width, height);
}

void CellularField::updateLivingCells() {
	alivePositions.clear();
	alivePositions.shrink_to_fit();

	for (int i = 0; i < width * height * depth; i++) {
		int x = i % width;
		int y = (i % (width * height)) / height;
		int z = i / (width * height);

		if (getCell(x, y, z)) {
			alivePositions.push_back(vec4(x * 2 + 1.0, y * 2 + 1.0, z * 2 + 1.0 - 2.3, 1.0));
		}
	}
}

int CellularField::getBackCell(unsigned int x, unsigned int y) const {
	return backBuffer[y * width + x];
}

int CellularField::getBackCell(ivec2 pos) const {
	return getBackCell(pos.x, pos.y);
}

int CellularField::getBackCell(unsigned int x, unsigned int y, unsigned int z) const {
	return backBuffer[z * width * height + y * width + x];
}

int CellularField::getBackCell(ivec3 pos) const {
	return getBackCell(pos.x, pos.y, pos.z);
}

int CellularField::getCell(unsigned int x, unsigned int y) const {
	return frontBuffer[y * width + x];
}

int CellularField::getCell(ivec2 pos) const {
	return getCell(pos.x, pos.y);
}

void CellularField::setCell(unsigned int x, unsigned int y, int val) {
	frontBuffer[y * width + x] = val;
}

void CellularField::setCell(ivec2 pos, int val) {
	setCell(pos.x, pos.y, val);
}

int CellularField::getCell(unsigned int x, unsigned  int y, unsigned  int z) const {
	return frontBuffer[z * width * height + y * width + x];
}

int CellularField::getCell(ivec3 pos) const {
	return getCell(pos.x, pos.y, pos.z);
}

void CellularField::setCell(unsigned int x, unsigned int y, unsigned int z, int val) {
	frontBuffer[z * width * height + y * width + x] = val;
}

void CellularField::setCell(ivec3 pos, int val) {
	setCell(pos.x, pos.y, pos.z, val);
}

void CellularField::randomize() {
	for (int i = 0; i < width * height * depth; i++) {
		this->frontBuffer[i] = rand() % 2;
	}
}

void CellularField::obviousPattern() {
	for (int i = 0; i < width * height; i++) {
		int x = i % width;
		int y = i / height;

		if (x == y || x == 0 || x == width-1 || y == 0 || y == height-1 || x + y == width - 1) {
			setCell(x, y, true);
		}
	}
}

void CellularField::update(double dt) {
    timePassed += dt;

    if (timePassed > UPDATE_DELAY) {
        backBuffer = frontBuffer;

        backBO.setData(sizeof(backBuffer[0]) * backBuffer.size(), &backBuffer[0], GL_DYNAMIC_DRAW);
        frontBO.setData(sizeof(frontBuffer[0]) * frontBuffer.size(), &frontBuffer[0], GL_DYNAMIC_DRAW);
        
        compute->use();
        compute->setUniform3i("dimensions", width, height, depth);
        backBO.bindBase(0);
        frontBO.bindBase(1);

        glDispatchCompute(width*height*depth, 1, 1);
        
        glMemoryBarrier(GL_SHADER_STORAGE_BARRIER_BIT);
        int *p = frontBO.mapData<int>(GL_MAP_READ_BIT);
        copy(p, p + frontBuffer.size(), &frontBuffer[0]);

        updateLivingCells();

        backBO.unbindBase(0);
        frontBO.unbindBase(1);

        timePassed = 0;
    }
}

void CellularField::draw(double dt) {

}


