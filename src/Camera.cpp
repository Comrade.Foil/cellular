#include <iostream>
#include "Camera.h"

Camera::Camera(const AppConfig& config) : config(config) {
	int width, height;
	
	position = config.cameraPosition;
	
	mouseSpd = 0.001f;
	camSpd = 10.0;
	horAngle = 3.14f;
	vertAngle = 0.0f;
	
	direction = calculateDirection();
	
	cursorStartX = width / 2;
	cursorStartY = height / 2;
	RMB_DOWN = false;
}

Camera::~Camera() {

}

mat4 Camera::getMVP() const {
	return MVP;
}

void Camera::update(GLdouble dTime, const Window& window) {
	double xpos = 0, ypos = 0;
	
	if (glfwGetMouseButton(window.windowContext, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS) {
		if (!RMB_DOWN) {
			glfwGetCursorPos(window.windowContext, &cursorStartX, &cursorStartY);
			glfwSetInputMode(window.windowContext, GLFW_CURSOR, GLFW_CURSOR_HIDDEN);
			RMB_DOWN = true;
		}
	} else {
		glfwSetInputMode(window.windowContext, GLFW_CURSOR, GLFW_CURSOR_NORMAL);
		RMB_DOWN = false;
	}

	if (RMB_DOWN) {
		glfwGetCursorPos(window.windowContext, &xpos, &ypos);

		horAngle += mouseSpd * double(cursorStartX - xpos);
		vertAngle += mouseSpd * double(cursorStartY - ypos);

		if (vertAngle > 3.14f / 2.0f) vertAngle = 3.14f / 2.0f;
		if (vertAngle < -3.14f / 2.0f) vertAngle = -3.14f / 2.0f;

		glfwSetCursorPos(window.windowContext, cursorStartX, cursorStartY);
	}
	
	direction = calculateDirection();
	
	// Right vector
	glm::vec3 right = glm::vec3(
		sin(horAngle - 3.14f / 2.0f),
		0,
		cos(horAngle - 3.14f / 2.0f)
	);
	right = glm::normalize(right);
	
	// Up vector
	glm::vec3 up = glm::cross(right, direction);
	up = glm::normalize(up);
	
	
	if (glfwGetKey(window.windowContext, GLFW_KEY_W) == GLFW_PRESS ||
		(glfwGetMouseButton(window.windowContext, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS &&
		 glfwGetMouseButton(window.windowContext, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS)) {
		position += direction * (camSpd * (GLfloat) dTime);
	}

	if (glfwGetKey(window.windowContext, GLFW_KEY_S) == GLFW_PRESS) {
		position -= direction * (camSpd * (GLfloat) dTime);

	}

	if (glfwGetKey(window.windowContext, GLFW_KEY_D) == GLFW_PRESS) {
		position += right * (camSpd * (GLfloat) dTime);
	}

	if (glfwGetKey(window.windowContext, GLFW_KEY_A) == GLFW_PRESS) {
		position -= right * (camSpd * (GLfloat) dTime);
	}

	if (glfwGetKey(window.windowContext, GLFW_KEY_E) == GLFW_PRESS) {
		position += vec3(0.0f, 1.0f, 0.0f) * (camSpd * (GLfloat) dTime);
	}

	if (glfwGetKey(window.windowContext, GLFW_KEY_Q) == GLFW_PRESS) {
		position += vec3(0.0f, -1.0f, 0.0f) * (camSpd * (GLfloat) dTime);
	}
	
	projMatrix = perspective(
		(float) config.fov,
		(float) config.windowWidth / (float) config.windowHeight,
		.1f,
		1500.0f
	);
//	projMatrix = ortho(-18.0, 18.0, -18.0, 18.0, 0.1, 100.0);
	
	viewMatrix = lookAt(position, position + direction, up);
	
	modelMatrix = mat4(1.0f);
	
	MVP = projMatrix * viewMatrix * modelMatrix;
}

mat4 Camera::getViewMatrix() const {
	return viewMatrix;
}

mat4 Camera::getProjectionMatrix() const {
	return projMatrix;
}

vec3 Camera::getPosition() const {
	return position;
}

vec3 Camera::calculateDirection() const {
	return normalize(vec3(
		cos(vertAngle) * sin(horAngle),
		sin(vertAngle),
		cos(vertAngle) * cos(horAngle)
	));
}

vec3 Camera::getDirection() const {
	return direction;
}



