#version 460

#define NEIGHBOURS_COUNT 26

layout(location=0) uniform ivec3 dimensions;

layout(std430, binding=0) buffer backBuffer {
    int back[];
};

layout(std430, binding=1) buffer frontBuffer {
    int front[];
};

layout(local_size_x = 1) in;

void main() {
    int index = int(gl_GlobalInvocationID);

    int size1d = dimensions.x;
    int size2d = dimensions.x * dimensions.y;
    int size3d = dimensions.x * dimensions.y * dimensions.z;

    bool isAlive = back[index] > 0;
    int directions[NEIGHBOURS_COUNT] = int[NEIGHBOURS_COUNT](
        -size2d-size1d-1, -size2d-size1d, -size2d-size1d+1, -size2d-1, -size2d, -size2d+1, -size2d+size1d-1, -size2d+size1d, -size2d+size1d+1,
        -size1d-1, -size1d, -size1d+1, 1, size1d+1, size1d, size1d-1, -1,
        size2d-size1d-1, size2d-size1d, size2d-size1d+1, size2d-1, size2d, size2d+1, size2d+size1d-1, size2d+size1d, size2d+size1d+1
    );

    int count = 0;

    for (int i = 0; i < NEIGHBOURS_COUNT; i++) {
        int pos = index + directions[i];

        if (pos > -1 && pos < size3d) {
            count += int(back[pos] > 0);
        }
    }

    front[index] = ((count < 5 && count > 3 && isAlive) || count > 6 && count < 10) ? count : 0;
}
