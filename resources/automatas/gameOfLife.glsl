#version 460

// Conway's game of life with periodic boundary

#define NEIGHBOURS_COUNT 8

layout(location=0) uniform ivec3 dimensions;

layout(std430, binding=0) buffer backBuffer {
    int back[];
};

layout(std430, binding=1) buffer frontBuffer {
    int front[];
};

layout(local_size_x = 1) in;

void main() {
    int index = int(gl_GlobalInvocationID);

    int size = dimensions.x * dimensions.y;
    bool isAlive = bool(back[index]);
    int directions[NEIGHBOURS_COUNT] = int[NEIGHBOURS_COUNT](
        -dimensions.x-1, -dimensions.x, -dimensions.x+1, 1, dimensions.x+1, dimensions.x, dimensions.x-1, -1
    );

    int count = 0;

    for (int i = 0; i < NEIGHBOURS_COUNT; i++) {
        int pos = (index + directions[i] + size) % size;
        count += back[pos];
    }

    front[index] = ((count==2 && isAlive) || count==3) ? 1 : 0;
}
