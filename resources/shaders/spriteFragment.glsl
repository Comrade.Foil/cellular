#version 460 core

in vec2 oTexCoord;

uniform float uAlpha;
uniform sampler2D colorTexture;

out vec4 result;

void main() {
    result = texture(colorTexture, oTexCoord);
    result = vec4(result.xyz, result.w * uAlpha);
//    result = vec4(0.0, 0.0, 0.0, 1.0);
}
