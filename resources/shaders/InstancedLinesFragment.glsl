#version 460 core

in vec4 outColor;

out vec4 result;

void main() {
    result = outColor;
}
