#version 460 core

layout(location = 0) in vec3 vertexPosition;
uniform mat4 uMVP;

void main() {
    vec4 pos = uMVP * vec4(vertexPosition, 1.0);

    gl_Position = pos;
}