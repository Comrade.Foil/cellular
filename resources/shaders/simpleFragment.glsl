#version 460

in vec4 oColor;

out vec4 fc;

void main() {
    fc = oColor;
}
