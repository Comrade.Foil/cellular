#version 460 core

layout(location = 0) in vec3 geometry;
layout(location = 1) in vec2 pointA;
layout(location = 2) in vec2 pointB;
layout(location = 3) in vec4 colorA;
layout(location = 4) in vec4 colorB;

uniform mat4 uMVP;
uniform float uWidth;
uniform mat4 uTransform;

out vec4 outColor;

void main() {
    vec2 xBasis = normalize(pointB - pointA);
    vec2 yBasis = normalize(vec2(-xBasis.y, xBasis.x));

    vec2 offsetA = pointA + uWidth * (geometry.x * xBasis + geometry.y * yBasis);
    vec2 offsetB = pointB + uWidth * (geometry.x * xBasis + geometry.y * yBasis);

    vec4 point = vec4(mix(offsetA, offsetB, geometry.z), 0.0, 1.0);

    gl_Position = uMVP * uTransform * point;
    outColor = mix(colorA, colorB, geometry.z);
}
