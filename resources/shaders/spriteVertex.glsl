#version 460 core

layout(location = 0) in vec4 geometry;
layout(location = 1) in vec2 texCoord;

uniform mat4 uMVP;
uniform mat4 uTransforms;

out vec2 oTexCoord;

void main() {
    vec4 p = uTransforms * geometry;

    gl_Position = uMVP * p;
    oTexCoord = texCoord;
}
