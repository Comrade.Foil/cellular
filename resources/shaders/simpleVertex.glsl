#version 460 core

layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec4 instanceOffset;

uniform mat4 uMVP;

out vec4 oColor;

float hash1( float n )
{
    return fract( n*17.0*fract( n*0.3183099 ) );
}

float hash2( float n )
{
    return fract( n*42.0*fract( n*1.3183099 ) );
}

float hash3( float n )
{
    return fract( n*42.0*fract( n*1.1231239 ) );
}

void main() {
    vec4 pos = uMVP * vec4(vec3(vertexPosition + instanceOffset.xyz).xyz, 1.0);

    gl_Position = pos;
    vec3 c = clamp(vec3(hash1(float(gl_InstanceID)), hash2(float(gl_InstanceID)), hash3(float(gl_InstanceID))), vec3(0.1), vec3(1));
    oColor = vec4(c, 0.4);
}
