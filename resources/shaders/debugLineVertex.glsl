#version 460

layout(location = 0) in vec3 iVertexPosition;
layout(location = 1) in vec4 iVertexColor;
layout(std430, binding = 1) buffer TransformsBuffer {
    mat4 iTransforms[];
};
layout(std430, binding = 2) buffer MappingBuffer {
    int iMappings[];
};
uniform mat4 uMVP;
uniform int uEntitiesCount;

out vec4 oVertexColor;

int getEntityTransformIndex() {
    for (int i = 0; i < uEntitiesCount; i++) {
        if (gl_VertexID < iMappings[i]) {
            return i;
        }
    }
    return uEntitiesCount - 1;
}

void main() {
    int transformIndex = getEntityTransformIndex();
    mat4 transform = iTransforms[transformIndex];
    vec4 pos = uMVP * transform * vec4(iVertexPosition, 1.0);

    gl_Position = pos;
    oVertexColor = iVertexColor;
}
