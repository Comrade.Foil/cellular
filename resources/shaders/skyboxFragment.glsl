#version 460 core
out vec4 oColor;

in vec3 TexCoords;

uniform samplerCube skybox;

void main()
{
    oColor = vec4(texture(skybox, TexCoords).xyz * 0.65, 1.0);
}