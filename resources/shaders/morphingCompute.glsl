#version 460 core

#define PI 3.141592653589
#define NUM_OCTAVES 2

uniform int uBufferSize;
uniform float uTime;

layout(std430, binding=0) buffer PointsBuffer {
    vec2 points[];
};

layout(std430, binding=1) buffer NormalsBuffer {
    vec2 circleNormals[];
};

layout(local_size_x = 1) in;

float random (in vec2 _st) {
    return fract(sin(dot(_st.xy,
    vec2(12.9898,78.233)))*
    43758.5453123);
}

float noise (in vec2 _st) {
    vec2 i = floor(_st);
    vec2 f = fract(_st);

    // Four corners in 2D of a tile
    float a = random(i);
    float b = random(i + vec2(1.0, 0.0));
    float c = random(i + vec2(0.0, 1.0));
    float d = random(i + vec2(1.0, 1.0));

    vec2 u = f * f * (3.0 - 2.0 * f);

    return mix(a, b, u.x) +
    (c - a)* u.y * (1.0 - u.x) +
    (d - b) * u.x * u.y;
}

float fbm ( in vec2 _st) {
    float v = 0.0;
    float a = 0.5;
    vec2 shift = vec2(100.0);
    // Rotate to reduce axial bias
    mat2 rot = mat2(cos(0.5), sin(0.5),
    -sin(0.5), cos(0.50));
    for (int i = 0; i < NUM_OCTAVES; ++i) {
        v += a * noise(_st);
        _st = rot * _st * 2.0 + shift;
        a *= 0.5;
    }
    return v;
}

float fbm2(float x) {
    float amplitude = 1.;
    float frequency = 1.;
    float y = sin(x * frequency);
    float t = 0.1*(-uTime*130.0);
    y += sin(x*frequency*2.1 + t)*4.5;
    y += sin(x*frequency*1.72 + t*1.121)*4.0;
    y += sin(x*frequency*2.221*sin(t) + t*0.437)*5.0;
    y += sin(x*frequency*3.1122+ t*4.269)*2.5;
    y *= amplitude*0.06;

    return y;
}

float fbm3(float x) {
    float tY = x;
    // Initial values
    float value = 0.0;
    float amplitude = .5;
    float frequency = 0.;
    //
    // Loop of octaves
    for (int i = 0; i < NUM_OCTAVES; i++) {
        value += amplitude * noise(vec2(x+uTime, uTime*0.3)).x;
        tY *= 2.;
        amplitude *= .5;
    }
    return value;
}

void main() {
    int index = int(gl_GlobalInvocationID);
    vec2 normal = circleNormals[index];
//    vec2 texCoord = vec2(float(index)/uBufferSize, float(index)/uBufferSize);
//    vec3 textureSample = normalize(texture(noiseTexture, texCoord).rgb);


    int virtualIndex = index % (uBufferSize - 1);
    float circleX = (virtualIndex/float(uBufferSize-1.0));
    float direction =  mix(1.0, -1.0, step(0.0, sin(uTime*0.5)));
    float scale = fbm2(circleX*4.0*PI + uTime) * 0.09;
    //float scale = fbm3(circleX*4.0*PI) * fbm2(uTime * circleX);


//    float offset = fbm2(circleX);


    //    normal = vec2(.0, 1.0);
//    points[index] = points[index] + 0.001*sin(uTime + (float(virtualIndex)/(uBufferSize - 1)) * 16.0*PI ) * circleNormals[index];
//    points[index] = pos + normal * 5.0*sin(float(index)/uBufferSize * 20.0*PI + uTime );
//    points[index] = points[index] + uBufferSize * normal;
    points[index] = points[index] + normal * scale;
//    points[index] = points[index] + textureSample.rg * 0.009*sin(uTime) * normal;

}
