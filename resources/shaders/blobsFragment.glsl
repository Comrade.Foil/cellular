#version 460 core

layout(std430, binding=0) buffer blobsPositions {
    vec4 positions[];// never use vec3 for SSBOs
};

uniform float uTime;
uniform int uBlobsAmount;
uniform mat4 uView;
uniform vec2 uResolution;
uniform vec3 uCameraPos;
uniform vec3 uCameraDir;

out vec4 oColor;

const int MAX_MARCHING_STEPS = 255;
const float MIN_DIST = 0.0;
const float MAX_DIST = 300.0;
const float EPSILON = 0.0001;

float sdSphere(vec3 marchingPosition, float radius) {
    return length(marchingPosition) - radius;
}

float opSmoothUnion(float d1, float d2, float k) {
    float h = clamp(0.5 + 0.5*(d2-d1)/k, 0.0, 1.0);
    return mix(d2, d1, h) - k*h*(1.0-h);
}

float opU(float o1, float o2)
{
    return min(o1, o2);
}

vec3 rayDirection(float fieldOfView, vec2 size, vec2 fragCoord, mat3 camRot) {
    vec2 xy = fragCoord - size / 2.0;
    float tanHalfFovy = tan(radians(fieldOfView) / 2.0);

    float z = (size.y / 2.0) / tanHalfFovy;
    return normalize(vec3(xy, z));
}

float map(vec3 p) {
    float d = 2.0;

    for (int i = 0; i < uBlobsAmount; i++) {
        d = opSmoothUnion(sdSphere(p - positions[i].xyz, 1.4 + 0.4 * sin((uTime/2.0) * i/5. + i)), d, 0.4);
    }

    return d;
}

float rayMarch(vec3 eye, vec3 direction, float start, float end) {
    float depth = start;

    for (int i = 0; i < MAX_MARCHING_STEPS; i++) {
        vec3 p = eye + depth * direction;
        float dist = map(p);

        if (dist < EPSILON) {
            return depth;
        }
        depth += dist;
        if (depth >= end) {
            return end;
        }
    }

    return end;
}

mat3 constructRoatationMatrix(vec3 camPos, vec3 camDir) {
    mat3 camRot = mat3(1.0);

    vec3 right = normalize(cross(camDir, vec3(0.0, 1.0, 0.0)));
    vec3 up = normalize(cross(right, camDir));
    vec3 forward = normalize(camDir);

    camRot[0][0] = right.x;
    camRot[0][1] = right.y;
    camRot[0][2] = right.z;

    camRot[1][0] = up.x;
    camRot[1][1] = up.y;
    camRot[1][2] = up.z;

    camRot[2][0] = forward.x;
    camRot[2][1] = forward.y;
    camRot[2][2] = forward.z;

    return camRot;
}

void main() {
    vec2 uv = gl_FragCoord.xy/uResolution.xy;
    mat3 camRot = constructRoatationMatrix(uCameraPos, uCameraDir);
    vec3 dir = camRot * rayDirection(58.0, uResolution, gl_FragCoord.xy, camRot);

    float dist = rayMarch(uCameraPos, dir, MIN_DIST, MAX_DIST);

    if (dist > MAX_DIST - EPSILON) {
        // Didn't hit anything
        oColor = vec4(0.0, 0.0, 0.0, 0.0);
        return;
    }

    oColor = vec4(vec3(dist), 1.0);
}
